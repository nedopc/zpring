/*  sprintem.cpp - main part of SprintEm (initially created on Feb 18, 2002).

    This file is part of SprintEm (emulator of Sprinter computer and clones).

    Copyright (c) 2002-2023, Alexander A. Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#ifndef WIN95
#ifndef __WATCOMC__
#include <png.h> // libpng
#define PNGOK
#endif
#endif
#ifdef SVGALIB
#error SVGALIB is not supported anymore
#include <vga.h>
#include <vgakeyboard.h>
#define getch vga_getch
#endif
#ifdef linux
#define stricmp strcasecmp
#endif
#ifdef __APPLE__
#define stricmp strcasecmp
#endif
#ifdef __WATCOMC__
#include <i86.h>
#include <conio.h>
#endif
#ifdef SDL
#include "SDL.h"
#include "SDL_timer.h"
#define delay(x) SDL_Delay(x)
#endif
#include "unigraf.h"
#include "sprintem.h"
#include "bios.h"
#include "z80/z80.h"
#include "z80/z_macros.h"

#define PRINTOUT
#define VIDEODEBUG

char fastpath[100] = "";

#ifdef linux
#ifndef SDL
#define delay(x) mydelay(x)
void mydelay(int x)
{int tt=(int)(clock()+x/1000.0*CLOCKS_PER_SEC);while(clock()<tt);} 
#endif
#endif

#define TITLE "Sprinter Emulator"

#define COPYRIGHT  "Copyright (C) 2002-2023, A.A.Shabarshin"

#define EMAIL "<me@shaos.net>"

#define COMMSTEP   100000
#define VIDELEN    128000
#ifdef BASICSE
#define INTERRUPTS 70
#endif

// Use mode 320x200 if you have only standard VGA card
#if 0
int VMODE = UG256_640x480;
#else
int VMODE = UG256_WINDOW;
#endif
int Windowed = 0;
int Debug = 0;
int SnapLast = 0;
int SnapDouble = 0;
unsigned int Start = 0;
unsigned int Stop = 0;
double Hz = 0;
unsigned long tstates = 0;
unsigned long tevent = 0;
BYTE *ram;
int ramw[4];
Page *ramo;
int RamPages = 0;
int Exit;
int ScrDX,ScrDY;
int Org = 0x8100;
int Stk = 0x80FF;
int Video = 0;
int VideoY = 0;
int VPage = 0;
int VPageZX = 5;
int OffsetX = 0;
int OffsetY = 0;
int AccelMode = 0;
int AccelSize = 0;
int AccelFlag = 0;
BYTE *AccelRAM;
BYTE *TextS;
BYTE *TextA;
BYTE *VCache;
BYTE VPorts[8] = {8,0,1,2,3,4,5,0}; // Extended
UniGraf *ug;
SprintemRGB Palette[8][256];
int CurrentPalette = 0;
char ExePath[256] = "";
int BiosEnabled = 1;
int EstexEnabled = 1;
int FastRamEnabled = 0;
int SpectrumEnabled = 0;
int Extended = 0;
int _FF = 0;
int _7FFD = 0;
int _1FFD = 0;
int RGMOD = 0;
int trickey = 0;
int directfile = 0;

int FreqEstex[256];
int FreqBios[256];
int FreqMouse[256];
int FreqOut[256];
int FreqIn[256];
int FreqPages[256];
int FreqMem[14]; // SETWINx4,PORTSx4,VIDPAGx4,BIOS,noBIOS

void SetStdPalette(int col);
void Print256(const char *s, int *a);
int SavePNG(char* filename, int width, int height, unsigned char* data);

const // GCC v8.3.0 complaints that XPM format uses char* for constant strings
#include "sprintem.xpm"

int LoadXPM(UniGraf* ug, int ox, int oy)
{
 char *po;
 int i,j,k,c,err,cc[128];
 int dx,dy,nc,nb;
 sscanf(sprintem_xpm[0],"%d %d %d %d",&dx,&dy,&nc,&nb);
 printf("Load XPM %dx%d (%d colors)\n",dx,dy,nc);
 if(nb!=1 || nc>16)
 {
   printf("Too many colors in XPM\n");
   return 0;
 }
 for(i=0;i<128;i++) cc[i]=0;
 err = 0;
 for(i=0;i<nc;i++)
 {
   po = (char*)sprintem_xpm[i+1];
   if(strstr(po,"None")) continue;
   c = po[0];
   po = strstr(po,"c #");
   if(po==NULL){err++;continue;}
   po += 3;
   if(po[0]=='0' && po[2]=='0' && po[4]=='0') cc[c] = 0;
   else if(po[0]=='0' && po[2]=='0' && po[4]=='A') cc[c] = 1;
   else if(po[0]=='0' && po[2]=='A' && po[4]=='0') cc[c] = 2;
   else if(po[0]=='0' && po[2]=='A' && po[4]=='A') cc[c] = 3;
   else if(po[0]=='A' && po[2]=='0' && po[4]=='0') cc[c] = 4;
   else if(po[0]=='A' && po[2]=='0' && po[4]=='A') cc[c] = 5;
   else if(po[0]=='A' && po[2]=='5' && po[4]=='0') cc[c] = 6;
   else if(po[0]=='A' && po[2]=='A' && po[4]=='A') cc[c] = 7;
   else if(po[0]=='5' && po[2]=='5' && po[4]=='5') cc[c] = 8;
   else if(po[0]=='5' && po[2]=='5' && po[4]=='F') cc[c] = 9;
   else if(po[0]=='5' && po[2]=='F' && po[4]=='5') cc[c] = 10;
   else if(po[0]=='5' && po[2]=='F' && po[4]=='F') cc[c] = 11;
   else if(po[0]=='F' && po[2]=='5' && po[4]=='5') cc[c] = 12;
   else if(po[0]=='F' && po[2]=='5' && po[4]=='F') cc[c] = 13;
   else if(po[0]=='F' && po[2]=='F' && po[4]=='5') cc[c] = 14;
   else if(po[0]=='F' && po[2]=='F' && po[4]=='F') cc[c] = 15;
   else err++;
 }
 if(err)
 {
    printf("Wrong format in XPM\n");
    return 0;
 }
 for(k=0;k<4;k++)
 {
   ug->Update();
   delay(500);
   for(j=0;j<dy;j++)
   {
     for(i=0;i<dx;i++)
     {
        c = cc[(int)sprintem_xpm[nc+1+j][i]];
        if(c<8)
        {
          if((k==2 && (i&1))||k<2) c=0;
        }
        else
        {
          switch(k)
          {
            case 0:
              if(i&1) c=0;
              else c-=8;
              break;
            case 1:
              c-=8;
              break;
            case 2:
              if(i&1) c-=8;
              break;
          }
        }
        ug->SetScreenPixel(ox+i,oy+j,c);
     }
   }
 }
 return 1;
}

FILE* UniFopen(const char *name, const char *fmode)
{
 FILE *f;
 int d = 0;
 char sfn[256];
 if(directfile && strstr(name,".EXE"))
 {
   d = directfile;
   strcpy(sfn,&name[d]);
   directfile = 0;
 }
 else sprintf(sfn,"%s%s",ExePath,name);
#if 0
 printf("UniFopen \"%s\" %s\n",sfn,fmode);
#endif
 f = fopen(sfn,fmode);
 if(f==NULL && d)
 {
    sprintf(sfn,"%s%s",ExePath,&name[d]);
    f = fopen(sfn,fmode);
 }
 return f;
}

/* Convert hexadecimal digit to integer */
int hexx(char c)
{
  int i = -1;
  switch(c)
  {
    case '0': i=0;break;
    case '1': i=1;break;
    case '2': i=2;break;
    case '3': i=3;break;
    case '4': i=4;break;
    case '5': i=5;break;
    case '6': i=6;break;
    case '7': i=7;break;
    case '8': i=8;break;
    case '9': i=9;break;
    case 'a': i=10;break;
    case 'b': i=11;break;
    case 'c': i=12;break;
    case 'd': i=13;break;
    case 'e': i=14;break;
    case 'f': i=15;break;
    case 'A': i=10;break;
    case 'B': i=11;break;
    case 'C': i=12;break;
    case 'D': i=13;break;
    case 'E': i=14;break;
    case 'F': i=15;break;
  }
  return i;
}

/* Convert string with hexadecimal number to integer */
int hex2i(char *s)
{
  char *ptr;
  long l = strtol(s,&ptr,16);
  if(*ptr) return -1;
  return (int)l;
}

#ifdef __WATCOMC__

/* Class to redirect stdout for DOS */

class Redirector
{
  FILE *f;
 public:
  Redirector(const char* fname);
 ~Redirector() { if(f) fclose(f); };
};

Redirector::Redirector(const char* fname)
{
  f = freopen(fname,"a",stdout);
  if(f==NULL)
  {
      fprintf(stderr,"\nUnable to redirect stdout!\n\n");
      exit(100);
  }
}

#endif

int main(int argc,char **argv)
{
 char str[256],st1[100],*po;
 char user[16] = "Emulator";
 int i,j,k,go;
 FILE *f;
 SprintEXE exe;
#ifdef __WATCOMC__
 Redirector MyStdOut("stdout.txt");
#endif
#ifdef PRINTOUT
 printf("\nSprintEm, " COPYRIGHT " " EMAIL "\n\n");
#endif
 for(i=0;i<256;i++)
 {
   FreqEstex[i]=0;
   FreqBios[i]=0;
   FreqMouse[i]=0;
   FreqOut[i]=0;
   FreqIn[i]=0;
   FreqPages[i]=0;
   if(i<14) FreqMem[i]=0;
 }
 strcpy(ExePath,argv[0]);
 po = strrchr(ExePath,'/');
 if(po==NULL) po = strrchr(ExePath,'\\');
 if(po!=NULL)
 {
    po++;
    *po = 0;
 }
 else *ExePath = 0;
 f = UniFopen("sprintem.ini","rt");
 if(f!=NULL)
 {
#ifdef PRINTOUT
   printf("sprintem.ini found!\n");
#endif
   while(1)
   {
     fgets(str,100,f);
     if(feof(f)) break;
     j = 0;
     for(i=0;i<(int)strlen(str);i++)
     {
       if(str[i]!='\r' && str[i]!='\n')  str[j++]=tolower(str[i]);
     }
     if(j==0) continue;
     if(*str=='/') continue;
     str[j] = 0;
     po = strchr(str,'=');
     if(po==NULL) continue;
     *po = 0;
     po++;
#ifdef PRINTOUT
     printf("Key '%s' has value '%s'\n",str,po);
#endif
     if(!stricmp(str,"debug")) Debug=atoi(po);
     else if(!stricmp(str,"stop")) Stop=atoi(po);
     else if(!stricmp(str,"start")) Start=atoi(po);
     else if(!stricmp(str,"shotlast")) SnapLast=atoi(po);
     else if(!stricmp(str,"shotdouble")) SnapDouble=atoi(po);
     else if(!stricmp(str,"mhz")) Hz=1e6*atof(po);
     else if(!stricmp(str,"a"))
     {
      while(*po=='.'||*po=='/'||*po=='\\'||*po==' ') po++;
      strcpy(PathA,po);
      k = strlen(PathA);
      if(k>=1 && PathA[k-1]!='/') strcat(PathA,"/");
      if(strstr(PathA,"..")) *PathA=0; // protection against relative path
      if(strchr(PathA,':')) *PathA=0; // protection against absolute path
     }
     else if(!stricmp(str,"video"))
     {
      if(!strcmp(po,"320x200")) VMODE=UG256_320x200;
      if(!strcmp(po,"640x400")) VMODE=UG256_640x400;
      if(!strcmp(po,"640x480")) VMODE=UG256_640x480;
      if(!strcmp(po,"window")) Windowed=640;
      if(!strcmp(po,"windowide")) Windowed=-640;
     }
     else if(!stricmp(str,"width"))
     {
      if(Windowed < 0)
         Windowed = -atoi(po);
      else
         Windowed = atoi(po);
     }
     else if(!stricmp(str,"extended")) Extended=atoi(po);
     else if(!stricmp(str,"board")) BoardID=atoi(po);
     else if(!stricmp(str,"user")) strncpy(user,po,15);
     else if(!stricmp(str,"autoexec"))
     {
         for(j=0;j<(int)strlen(po);j++)
         {
            if(j>=99) break;
            fastpath[j] = toupper(po[j]);
         }
         fastpath[j] = 0;
     }
     else printf("Error %s!\n",str);
   }
   fclose(f);
 }
 user[15] = 0;
#ifdef NETWORK
 NetworkInit(user);
#endif
 ram = new BYTE[RAMSIZE];
 if(ram==NULL)
 {
   printf("\n\nOut of memory 1\n\n");
   return -1;
 }
 memset(ram,0,RAMSIZE);
 RamPages = RAMSIZE>>14;
#ifdef PRINTOUT
 printf("RamSize  = %i\n",RAMSIZE);
 printf("RamPages = %i\n",RamPages);
#endif
 ramo = new Page[RamPages];
 if(ramo==NULL)
 {
   delete ram;
   printf("\n\nOut of memory 2\n\n");
   return -1;
 }
 for(i=0;i<RamPages;i++) 
 {
   ramo[i].data = &ram[i<<14];
   ramo[i].occ = 0;
 }
 TextS = new BYTE[TEXTLEN];
 if(TextS==NULL)
 {
   delete ram;
   delete ramo;
   printf("\n\nOut of memory 3\n\n");
   return -1;
 }
 TextA = new BYTE[TEXTLEN];
 if(TextA==NULL)
 {
   delete ram;
   delete ramo;
   delete TextS;
   printf("\n\nOut of memory 4\n\n");
   return -1;
 }
 for(i=0;i<TEXTLEN;i++)
 {
   TextS[i] = 0;
   TextA[i] = 7;
 }
 VCache = new BYTE[VIDELEN];
 if(VCache==NULL)
 {
   delete ram;
   delete ramo;
   delete TextS;
   delete TextA;
   printf("\n\nOut of memory 5\n\n");
   return -1;
 }
 srand(time(NULL));
 for(i=0;i<VIDELEN;i++) VCache[i]=rand()&255;
 if(!Extended) AccelRAM = new BYTE[256];
 else AccelRAM = new BYTE[65536];
 if(AccelRAM==NULL)
 {
   delete ram;
   delete ramo;
   delete TextS;
   delete TextA;
   delete VCache;
   printf("\n\nOut of memory 6\n\n");
   return -1;
 }

 z80_init(Debug);
 Init(); // init estex

 go = 1;
 while(go) // run once
 {
  go = 0;
  printf("ExePath=%s\n",ExePath);
  if(argc>1 || *fastpath)
  {
   if(argc>1) *fastpath=0;
   strcpy(str,Path);
   k = (int)strlen(str);
   if(*fastpath) strcat(str,fastpath);
   else
   {
      strcat(str,argv[1]);
      for(i=k;i<(int)strlen(str);i++) str[i]=toupper(str[i]);
   }
   po = strchr(str,' ');
   if(po!=NULL) *po=0;
   printf("Execute \"%s\"\n",str);
   f = UniFopen(str,"rb");
   if(f==NULL)
   {
     po = &str[k];
     f = fopen(po,"rb");
     if(f!=NULL) directfile = k;
     else
     {
        strcpy(st1,ExePath);
        strcat(st1,&str[k]);
        f = fopen(st1,"rb");
        if(f!=NULL) directfile = k;
        else
        {
           printf("\n\nCan't open file %s\n\n",str);
           break;
        }
     }
   }
   fseek(f,0,SEEK_END);
   k = ftell(f);
   fseek(f,0,SEEK_SET);
   fread(&exe,1,sizeof(SprintEXE),f);
   if(!memcmp(exe.id,"EXE",3))
   {
#ifdef PRINTOUT
	printf("EXE.version=#%2.2X\n",exe.version);
	printf("EXE.offset=#%4.4X\n",(int)exe.offset);
	printf("EXE.loader=#%4.4X\n",exe.loader);
	printf("EXE.load=#%4.4X\n",exe.load);
	printf("EXE.start=#%4.4X\n",exe.start);
	printf("EXE.stack=#%4.4X\n",exe.stack);
#endif
	fclose(f);
	HL = 0x1000;
	k = HL;
	for(i=1;i<argc;i++)
	{
	    for(j=0;j<(int)strlen(argv[i]);j++) writebyte(k++,argv[i][j]);
	    writebyte(k++,' ');
	}
	if(*fastpath)
	{
	    for(j=0;j<(int)strlen(fastpath);j++) writebyte(k++,fastpath[j]);
	    writebyte(k++,' ');
	}
	writebyte(k-1,0);
	B = 0;
	Estex(0x40);
   }
   else
   {
	if(argc>2) Org=hex2i(argv[2]);
	if(argc>3) Stk=hex2i(argv[2]);
	fseek(f,0,SEEK_SET);
	for(i=0;i<k;i++) writebyte(Org+i,fgetc(f));
	PC = Org;
	SP = Stk;
	fclose(f);
	Level++;
   }
  }
 }

 if(Windowed<0)
      ug = new UniGraf(VMODE,-Windowed,((-Windowed)>>4)*9);
 else ug = new UniGraf(VMODE,Windowed);
 if(ug==NULL) return 0;
#ifdef SDL
 if(Windowed) SDL_WM_SetCaption(TITLE,TITLE);
#endif
 if(!ug->Screen(640,256)) printf("Screen Error!\n");
 SetStdPalette(16);
 if(!Extended)
      SetVideo(0x03);
 else SetVideo(0x84);
 ug->DrawString(320,2,"Sprinter Emulator (" __DATE__ ")",8,11,0);
 ug->DrawString(320,12,"comes with ABSOLUTELY NO WARRANTY;",8,3,0);
 ug->DrawString(320,22,"This is free software, and you are",8,3,0);
 ug->DrawString(320,32,"welcome to redistribute it under certain",8,3,0);
 ug->DrawString(320,42,"conditions; see GPLv2 for details",8,3,0);
 ug->DrawString(320,52,"https://gitlab.com/nedopc/sprintem",8,9,0);
 ug->DrawString(320,62,COPYRIGHT,8,10,0);
 ug->DrawString(320,72,"Z80 emulation was borrowed from FUSE",8,2,0);
 ug->DrawString(320,82,"Copyright (C) 1999-2000 Philip Kendall",8,2,0);
 LoadXPM(ug,4,4);
#if 0
 if(Extended && argc==1)
 {
  int i1,j1;
  for(i=0;i<33;i++){
  for(j=0;j<17;j++){
   for(i1=0;i1<16;i1++){
   for(j1=0;j1<12;j1++){
     if(i==0||j==0) SetPixel(i*18+i1+24,j*14+j1+56,(i+j-1)&15);
     else if(i<=16)
     {
        if((i1+j1)&1)
             k=i-1;
        else k=j-1;
        SetPixel(i*18+i1+24,j*14+j1+56,k);
     }
     else
     {
        if((i1&1)&&((i1/2+j1)&1))
             k=i-17;
        else k=j-1;
        SetPixel(i*18+i1+24,j*14+j1+56,k);
     }
   }}
  }}
  for(i=0;i<33;i++)
  {
   if(i==0) ug->DrawChar8x8(28,59,'$',0,15);
   else
   {
     k = (i-1)&15;
     if(k==0)
        ug->DrawChar8x8(28+i*18,59,'0',8);
     else if(k<10)
        ug->DrawChar8x8(28+i*18,59,'0'+k,0);
     else
        ug->DrawChar8x8(28+i*18,59,'A'+k-10,0);
   }
  }
  for(j=1;j<17;j++)
  {
   if(j==1)
      ug->DrawChar8x8(28,59+j*14,'0',8);
   else if(j<11)
      ug->DrawChar8x8(28,59+j*14,'0'+j-1,0);
   else
      ug->DrawChar8x8(28,59+j*14,'A'+j-11,0);
  }
  ug->DrawString(1<<3,301,"SprintEm comes with ABSOLUTELY NO WARRANTY; This is free software, and you are",8,8,0);
  ug->DrawString(3<<3,311,"welcome to redistribute it under certain conditions; see GPLv2 for details",8,8,0);
  ug->DrawString(17<<3,331,"Press Shift-F8 to toggle \"de-dithering\" effect",8,7,0);
 }
#endif
 ug->Update();
 CurY = 12;

 delay(3000);

 if(fdebug!=NULL) fprintf(fdebug,"\nStart #%4.4X\n",PC);

 double d;
 unsigned long upi = 0;
 unsigned long t1,t2,ts;
#ifdef INTERRUPTS
 unsigned long ti;
 int ipers = 1;
 if(CLOCKS_PER_SEC > INTERRUPTS) ipers=CLOCKS_PER_SEC/INTERRUPTS;
#endif
 t1 = clock();
 ts = tstates;
#ifdef INTERRUPTS
 ti = t1+ipers;
#endif
#ifdef BASICSE
 char scom[] = "MODE 1\n";//10 PRINT \"HELLO\"\n`RUN\n";
 int icom = 0;
 unsigned long icurrent=0,inext=5;
#endif
 unsigned int tsec = time(NULL), tbils = 0;
 unsigned long tlast = 0;
 while(!Exit)
 {
   if(z80.halted)
   {
      z80.halted = 0;
   }

   if(tstates - upi > COMMSTEP)
   {
      ug->Update();
      if(ug->KeyPressed(SCANCODE_LEFTSHIFT)||ug->KeyPressed(SCANCODE_RIGHTSHIFT))
      {
        if(ug->KeyPressed(SCANCODE_F2)) { MemorySave(); delay(500); }
        if(ug->KeyPressed(SCANCODE_F8)) { SwitchMode(); delay(500); }
        if(ug->KeyPressed(SCANCODE_F9)) { ScreenShot(); delay(500); }
        if(ug->KeyPressed(SCANCODE_F10)) break;
      }
      upi = tstates;
      if(Hz > 1 && (unsigned long)clock() > t1)
      {
        d = (tstates-ts)/Hz;
        t2 = t1 + (unsigned long)(d*CLOCKS_PER_SEC);
        while((unsigned long)clock()<t2);
        t1 = clock();
        ts = tstates;
      }
#ifdef INTERRUPTS
      if(t1 > ti)
      {
#ifdef BASICSE
         if(++icurrent > inext)
         {
           printf("%lu) trickey = #%2.2X (%i)\n",tstates,trickey,icom);
#if 0
           if(scom[icom]=='L') MemorySave(1);
           if(scom[icom]=='S') MemorySave(2);
#endif
           if(scom[icom])
           {
              trickey = scom[icom++];
              if(trickey=='`')
                   inext = icurrent + 70;
              else inext = icurrent + 3;
           }
           else
           {
              trickey = 0;
              inext = icurrent + 70*3600;
           }
         }
#endif
         z80_interrupt();
         ti = t1+ipers;
      }
#endif
#if 0
      char sout[16];
      static unsigned char cout = 255;
      sprintf(sout,"[%4.4X]=%2.2X ",PC,readbyte(PC));
      ug->DrawString(0,0,sout,8,cout,0);
#endif
   }
   tevent += 20;
   z80_do_opcodes(Debug && (tstates>=Start*1000L));
   if(Stop>0 && tstates>Stop*1000L)
   {
      if(Debug && fdebug!=NULL)
      {  Debug = 0;
         fclose(fdebug);
         fdebug = NULL;
      }
      break;
   }
/*
   if(tstates>=0xFFFFFF00)
   {
      tevent = tstates = 0;
   }
*/
   if(tstates >= tlast + 1000000000L)
   {
      tbils++;
      tlast = tstates;
      unsigned long nt = time(NULL);
      sprintf(str,"%s %ibln ~%3.2fMHz",TITLE,tbils,1000.0/(nt-tsec));
      tsec = nt;
      printf(">>> %s\n",str);
#ifdef SDL
      if(Windowed) SDL_WM_SetCaption(str,TITLE);
#endif
   }
 }

 ug->Update();
 ScreenShot();
 delay(1000);
#if 1
 MemorySave(-1); // Save ALL memory
#endif
 z80_stop(Debug);
 ug->Close();
 delete ug;
 delete ram;
 delete ramo;
 delete TextS;
 delete TextA;
 delete VCache;
 delete AccelRAM;

#if 1
 Print256("Estex Statistics",FreqEstex);
 Print256("Bios Statistics",FreqBios);
 Print256("Mouse Statistics",FreqMouse);
 Print256("Port In Statistics",FreqIn);
 Print256("Port Out Statistics",FreqOut);
 Print256("Pages Statistics",FreqPages);
 printf("\n");
 printf("SETWIN:%i,%i,%i,%i\n",FreqMem[0],FreqMem[1],FreqMem[2],FreqMem[3]);
 printf("PORTS:%i,%i,%i,%i\n",FreqMem[4],FreqMem[5],FreqMem[6],FreqMem[7]);
 printf("VIDEO:%i,%i,%i,%i\n",FreqMem[8],FreqMem[9],FreqMem[10],FreqMem[11]);
 printf("BIOS:%i, noBIOS:%i\n",FreqMem[12],FreqMem[13]);
#endif

 return 0;
}

int VideoSwap(int vid)
{
//return 0;
 int i,j,k,t,b;
 if(VCache==NULL) return 0;
 if(Extended && vid==0x81) vid=0x85;
 if(Extended && vid==0x82) vid=0x84;
 switch(vid)
 {
   case 0x83: // VPage?
     k = 0;
     for(j=0;j<200;j++){
     for(i=0;i<320;i++){
       t = GetPixel(i,j);
       SetPixel(i,j,VCache[k]);
       VCache[k++] = t;
     }}
     break;

   case 0x84: // Vports?
     k = 0;
     for(j=0;j<350;j++){
     for(i=0;i<320;i++){
       t = ((GetPixel(2*i,j+OffsetY)&15)<<4)|(GetPixel(2*i+1,j+OffsetY)&15);
       b = VCache[k];
       SetPixel(2*i,j,b>>4);
       SetPixel(2*i+1,j,b&15);
       VCache[k++] = t;
     }}
     break;

   case 0x85: // VPorts?
     k = 0;
     for(j=0;j<350;j++){
     for(i=0;i<320;i++){
       t = GetPixel(i,j+OffsetY);
       SetPixel(i,j,VCache[k]);
       VCache[k++] = t;
     }}
     break;

   case 0x86:
     k = 0;
     for(j=0;j<400;j++){
     for(i=0;i<320;i++){
       t = ((GetPixel(2*i,j)&15)<<4)|(GetPixel(2*i+1,j)&15);
       b = VCache[k];
       SetPixel(2*i,j,b>>4);
       SetPixel(2*i+1,j,b&15);
       VCache[k++] = t;
     }}
     break;

   case 0x87:
     k = 0;
     for(j=0;j<400;j++){
     for(i=0;i<320;i++){
       t = GetPixel(i,j);
       SetPixel(i,j,VCache[k]);
       VCache[k++] = t;
     }}
     break;

 }
 return 1;
}



int VideoBackup(int vid)
{
 int i,j,k;
 if(VCache==NULL) return 0;
 if(Extended && vid==0x81) vid=0x85;
 if(Extended && vid==0x82) vid=0x84;
 switch(vid)
 {
   case 0x83: // VPage?
     k = 0;
     for(j=0;j<200;j++){
     for(i=0;i<320;i++){
       VCache[k++] = GetPixel(i,j);
     }}
     break;

   case 0x84: // Vports?
     k = 0;
     for(j=0;j<350;j++){
     for(i=0;i<320;i++){
       VCache[k++] = ((GetPixel(2*i,j+OffsetY)&15)<<4)|(GetPixel(2*i+1,j+OffsetY)&15);
     }}
     break;

   case 0x85: // VPorts?
     k = 0;
     for(j=0;j<350;j++){
     for(i=0;i<320;i++){
       VCache[k++] = GetPixel(i,j+OffsetY);
     }}
     break;

   case 0x86:
     k = 0;
     for(j=0;j<400;j++){
     for(i=0;i<320;i++){
       VCache[k++] = ((GetPixel(2*i,j)&15)<<4)|(GetPixel(2*i+1,j)&15);
     }}
     break;

   case 0x87:
     k = 0;
     for(j=0;j<400;j++){
     for(i=0;i<320;i++){
       VCache[k++] = GetPixel(i,j);
     }}
     break;

 }
 return 1;
}

int VideoRestore(int vid)
{
 int i,j,k,b;
 if(VCache==NULL) return 0;
 switch(vid)
 {
   case 0x83: // VPage?
     k = 0;
     for(j=0;j<200;j++){
     for(i=0;i<320;i++){
       SetPixel(i,j,VCache[k++]);
     }}
     break;

   case 0x84: // VPorts?
     k = 0;
     for(j=0;j<350;j++){
     for(i=0;i<320;i++){
       b = VCache[k++];
       SetPixel(2*i,j,b>>4);
       SetPixel(2*i+1,j,b&15);
     }}
     break;

   case 0x85: // VPorts?
     k = 0;
     for(j=0;j<350;j++){
     for(i=0;i<320;i++){
        SetPixel(i,j,VCache[k++]);
     }}
     break;

   case 0x86:
     k = 0;
     for(j=0;j<400;j++){
     for(i=0;i<320;i++){
       b = VCache[k++];
       SetPixel(2*i,j,b>>4);
       SetPixel(2*i+1,j,b&15);
     }}
     break;

   case 0x87:
     k = 0;
     for(j=0;j<400;j++){
     for(i=0;i<320;i++){
       SetPixel(i,j,VCache[k++]);
     }}
     break;

 }
 return 1;
}

int VideoWrite(int page,int offs,int byte)
{
 int i,j,k,a,s,b,c,b1,b2,VideoX;
 if((page<0x50 || page>0x5F)
#ifdef BASICSE
  && page!=5 && page!=7
#endif
   ) return 1;
 VideoX = offs;
#ifdef VIDEODEBUG
 if(fdebug!=NULL) fprintf(fdebug,"Video%2.2X/%2.2X(%i:%i)=%2.2X ",Video,page,VideoX,VideoY,byte);
#endif
 if(page>=0x50 && page<=0x5F)
 {
  int TransparentMode = (page & 8)?1:0;
  int TemporaryMode   = (page & 4)?1:0;

  if(Video==0x03)
  {
    k = VideoX-768; // ???
    i = VideoY-129; // ???
    j = k>>2;
    a = i+j*80;
    if(a<0 || a>=TEXTLEN) return 0;
    if((k&3)==2) TextA[a]=byte;
    if((k&3)==1) TextS[a]=byte;
    c = TextS[a];
    if(c==0) c=' ';
    s = (TextA[a] & 0x0F);
    b = (TextA[a] & 0x70) >> 4;
    PrintChar(i,j,c,s,b,0);
    return TemporaryMode?0:1; // ???
  }
  else if(Video==0x81||Video==0x83||Video==0x85||Video==0x87)
  {
    if(VideoX>=992 && !Extended)
    {
      int pi = 0;
      switch(VideoX)
      {
         case 992: Palette[0][VideoY].Red   = byte; pi = 0; break;
         case 993: Palette[0][VideoY].Green = byte; pi = 0; break;
         case 994: Palette[0][VideoY].Blue  = byte; pi = 0; break;
         case 996: Palette[1][VideoY].Red   = byte; pi = 1; break;
         case 997: Palette[1][VideoY].Green = byte; pi = 1; break;
         case 998: Palette[1][VideoY].Blue  = byte; pi = 1; break;
         case 1000: Palette[2][VideoY].Red   = byte; pi = 2; break;
         case 1001: Palette[2][VideoY].Green = byte; pi = 2; break;
         case 1002: Palette[2][VideoY].Blue  = byte; pi = 2; break;
         case 1004: Palette[3][VideoY].Red   = byte; pi = 3; break;
         case 1005: Palette[3][VideoY].Green = byte; pi = 3; break;
         case 1006: Palette[3][VideoY].Blue  = byte; pi = 3; break;
      }
      ug->SetPalette(VideoY,Palette[pi][VideoY].Red,Palette[pi][VideoY].Green,Palette[pi][VideoY].Blue);
// hack begin
      if(pi==1)
      {
         Palette[0][VideoY].Red   = Palette[pi][VideoY].Red;
         Palette[0][VideoY].Green = Palette[pi][VideoY].Green;
         Palette[0][VideoY].Blue  = Palette[pi][VideoY].Blue;
      }
// hack end
      return TemporaryMode?0:1;
    }
    else
    {
      if(VideoX > OffsetX && (!TransparentMode || byte!=0xFF))
         ug->SetScreenPixel(VideoX-OffsetX,VideoY+OffsetY,byte);
    }
    return TemporaryMode?0:1;
  }
  else if(Video==0x82||Video==0x84||Video==0x86)
  {
    if(VideoX > OffsetX && (!TransparentMode || byte!=0xFF)) // ???
    {
      b1 = byte >> 4;
      b2 = byte & 0xF;
      VideoX <<= 1; // it is local variable
      ug->SetScreenPixel(VideoX-OffsetX,VideoY+OffsetY,b1);
      ug->SetScreenPixel(VideoX-OffsetX+1,VideoY+OffsetY,b2);
    }
    return TemporaryMode?0:1; // ???
  }
 }
#ifdef BASICSE
 else if(SpectrumEnabled && (page==5||page==7))
 {
//  if(byte) printf("Page %i write 0x%4.4X = 0x%2.2X (7FFD=0x%2.2X,FF=0x%2.2X)\n",page,offs,byte,_7FFD,_FF);
  int b,i,j,k,p=0,y=0,x=-1;
  if(page==7 && ((_FF&7)==6) && offs>=8192 && offs<14336)
  {
    offs-=8192;
    p = 1;
  }
  if(offs<6144)
  {
    i = offs>>11;
    j = (offs&2047)>>8;
    k = (offs&255)>>5;
    x = offs&31;
    y = 79+((i<<6)|(k<<3)|j);
  }
  else return 1;
  switch(_FF&7)
  {
    case 0:
    case 2:
      if(x>=0 && page==VPageZX)
      {
        b = byte;
        for(i=0;i<8;i++)
        {
          if(b&0x80)
          {
             ug->SetScreenPixel(64+(x<<4)+(i<<1),y,15);
             ug->SetScreenPixel(64+(x<<4)+(i<<1)+1,y,15);
          }
          else
          {
             ug->SetScreenPixel(64+(x<<4)+(i<<1),y,0);
             ug->SetScreenPixel(64+(x<<4)+(i<<1)+1,y,0);
          }
          b<<=1;
        }
      }
      break;
    case 6:
      if(x>=0)
      {
        x<<=1;
        if(p) x++;
        b = byte;
        for(i=0;i<8;i++)
        {
          if(b&0x80)
             ug->SetScreenPixel(64+(x<<3)+i,y,15);
          else
             ug->SetScreenPixel(64+(x<<3)+i,y,0);
          b<<=1;
        }
      }
      break;
  }
  return 1;
 }
#endif
 return 1;
}

int VideoRead(int page,int offs)
{
 int VideoX,k,x,y,a=0,byte=0;
 int TransparentMode = (page & 8)?1:0;
 int TemporaryMode   = (page & 4)?1:0;
 if(page<0x50 || page>0x5F || TransparentMode || TemporaryMode) return -1;
 VideoX = offs;

  if(Video==0x03)
  {
    k = VideoX-768; // ???
    x = VideoY-129; // ???
    y = k>>2;
    a = x+y*80;
    if(a>=0 && a<TEXTLEN)
    {
      if((k&3)==2) byte=TextA[a];
      if((k&3)==1) byte=TextS[a];
    }
  }
  else if(Video==0x81||Video==0x83||Video==0x85||Video==0x87)
  {
    if(VideoX>=992 && !Extended)
    {
      if(VideoX==992) byte = Palette[0][VideoY].Red;
      if(VideoX==993) byte = Palette[0][VideoY].Green;
      if(VideoX==994) byte = Palette[0][VideoY].Blue;
      return 1;
    }
    else
    {
       VideoX -= OffsetX;
       if(VideoX>=0 && VideoX<ScrDX && VideoY>=0 && VideoY<ScrDY)
         byte=ug->GetScreenPixel(VideoX,VideoY+OffsetY);
    }
  }
  else if(Video==0x82||Video==0x84||Video==0x86)
  {
    VideoX -= OffsetX;
    if(VideoX>=0 && VideoX<ScrDX && VideoY>=0 && VideoY<ScrDY)
       byte =  ((ug->GetScreenPixel(VideoX<<1,VideoY+OffsetY)&0xF)<<4) |
                (ug->GetScreenPixel((VideoX<<1)+1,VideoY+OffsetY)&0xF);
  }

#ifdef VIDEODEBUG
 if(fdebug!=NULL) fprintf(fdebug,"VRead%2.2X/%2.2X(%i:%i)=%2.2X ",Video,page,VideoX,VideoY,byte);
#endif
 return byte;
}

BYTE readbyte(WORD address,BYTE flag)
{
 short bb;
 BYTE b = 0;
 WORD ad = address&0x3FFF;
#ifdef BASICSE
 if((ramw[3]&7)==7 && address>=0x4000 && address<=0x8000)
   printf("readbyte #%4.4X while page 7 is active (PC=#%4.4X SP=#%4.4X)\n",address,z80.pc,z80.sp);
#endif
 switch(address&0xC000)
 {
   case 0x0000:
        bb = VideoRead(ramw[0],ad);
        if(bb>=0) b=bb;
        else
        b=ramo[ramw[0]].data[ad];
        break;
   case 0x4000:
        bb = VideoRead(ramw[1],ad);
        if(bb>=0) b=bb;
        else
        b=ramo[ramw[1]].data[ad];
        break;
   case 0x8000:
        bb = VideoRead(ramw[2],ad);
        if(bb>=0) b=bb;
        else
        b=ramo[ramw[2]].data[ad];
        break;
   case 0xC000:
        bb = VideoRead(ramw[3],ad);
        if(bb>=0) b=bb;
        else
        b=ramo[ramw[3]].data[ad];
        break;
 }
 if(AccelMode && flag && !AccelFlag) Accel('R',b,address);
 return b;
}

BYTE writebyte(WORD address,BYTE data)
{
 BYTE b = 0;
 WORD ad = address&0x3FFF;
#ifdef BASICSE
 if((ramw[3]&7)==7 && address>=0x4000 && address<=0x8000)
   printf("writebyte #%2.2X to #%4.4X while page 7 is active (PC=#%4.4X SP=#%4.4X)\n",data,address,z80.pc,z80.sp);
#endif
 switch(address&0xC000)
 {
   case 0x0000:
        b = ramo[ramw[0]].data[ad];
        ramo[ramw[0]].data[ad] = data;
        if(!VideoWrite(ramw[0],ad,data))
           ramo[ramw[0]].data[ad] = b;
        break;
   case 0x4000:
        b = ramo[ramw[1]].data[ad];
        ramo[ramw[1]].data[ad] = data;
        if(!VideoWrite(ramw[1],ad,data))
           ramo[ramw[1]].data[ad] = b;
        break;
   case 0x8000:
        b = ramo[ramw[2]].data[ad];
        ramo[ramw[2]].data[ad] = data;
        if(!VideoWrite(ramw[2],ad,data))
           ramo[ramw[2]].data[ad] = b;
        break;
   case 0xC000:
        b = ramo[ramw[3]].data[ad];
        ramo[ramw[3]].data[ad] = data;
        if(!VideoWrite(ramw[3],ad,data))
           ramo[ramw[3]].data[ad] = b;
        break;
 }
 if(AccelMode && !AccelFlag) Accel('W',data,address);
 return b;
}

BYTE readport(WORD port)
{
 BYTE b = 0xFF;
 BYTE po = port & 0xFF;
 BYTE hi = port >> 8;
 FreqIn[po]++;
 switch(po)
 {
   case PORT_PAGE_0: b=ramw[0]; break; // 0x82
   case PORT_PAGE_1: b=ramw[1]; break; // 0xA2
   case PORT_PAGE_2: b=ramw[2]; break; // 0xC2
   case PORT_PAGE_3: b=ramw[3]; break; // 0xE2
   case PORT_Y: b=VideoY&255; break; // 0x89
   case PORT_Y1: if(VideoY<256) b=0; else b=VideoY-256; break; // 0x8A
   case 0x18: b=ug->WhatKey(); break;
   case 0x19: if(ug->Update()) b=1; break;
   case 0xC9:
#ifdef PRINTOUT
        printf("Read port 0xC9 - RGMOD (%2.2X)\n",RGMOD);
#endif
        b = RGMOD;
        break;
   case 0x7B:
#ifdef PRINTOUT
        printf("Read port 0x7B - FastRAM disabled!\n");
#endif
        FastRamEnabled = 0;
        break;
   case 0xFB:
#ifdef PRINTOUT
        printf("Read port 0xFB - FastRAM enabled!\n");
#endif
        FastRamEnabled = 1;
        break;
   case 0xFD:
        if(hi==0x1F)
        {
#ifdef PRINTOUT
           printf("Read port 0x1FFD\n");
#endif
           b = _1FFD;
        }
        else if(hi==0x7F)
        {
#ifdef PRINTOUT
           printf("Read extended port 0x7FFD = 0x%2.2X\n",b);
#endif
           b = _7FFD;
        }
        else if(hi==0xFF)
        {
#ifdef PRINTOUT
           printf("read AY-date from port 0xFFFD\n");
#endif
        }
        else printf("Read from unknown port 0x%4.4X\n",port);
        break;
   case 0xFE:
#ifdef BASICSE
// 0xfefe  SHIFT, Z, X, C, V
// 0xfdfe  A, S, D, F, G
// 0xfbfe  Q, W, E, R, T
// 0xf7fe  1, 2, 3, 4, 5
// 0xeffe  0, 9, 8, 7, 6
// 0xdffe  P, O, I, U, Y
// 0xbffe  ENTER, L, K, J, H
// 0x7ffe  SPACE, SYM SHFT, M, N, B
        switch(hi)
        {
          case 0xFE:
               if(ug->KeyPressed(42)||ug->KeyPressed(54)||ug->KeyPressed(14)||trickey==1) b-=1;
               if(ug->KeyPressed(44)||trickey=='Z') b-=2;
               if(ug->KeyPressed(45)||trickey=='X') b-=4;
               if(ug->KeyPressed(46)||trickey=='C') b-=8;
               if(ug->KeyPressed(47)||trickey=='V') b-=16;
               break;
          case 0xFD:
               if(ug->KeyPressed(30)||trickey=='A') b-=1;
               if(ug->KeyPressed(31)||trickey=='S') b-=2;
               if(ug->KeyPressed(32)||trickey=='D') b-=4;
               if(ug->KeyPressed(33)||trickey=='F') b-=8;
               if(ug->KeyPressed(34)||trickey=='G') b-=16;
               break;
          case 0xFB:
               if(ug->KeyPressed(16)||trickey=='Q') b-=1;
               if(ug->KeyPressed(17)||trickey=='W') b-=2;
               if(ug->KeyPressed(18)||trickey=='E') b-=4;
               if(ug->KeyPressed(19)||trickey=='R') b-=8;
               if(ug->KeyPressed(20)||trickey=='T') b-=16;
               break;
          case 0xF7:
               if(ug->KeyPressed(2)||trickey=='1') b-=1;
               if(ug->KeyPressed(3)||trickey=='2') b-=2;
               if(ug->KeyPressed(4)||trickey=='3') b-=4;
               if(ug->KeyPressed(5)||trickey=='4') b-=8;
               if(ug->KeyPressed(6)||trickey=='5') b-=16;
               break;
          case 0xEF:
               if(ug->KeyPressed(11)||ug->KeyPressed(14)||trickey=='0') b-=1;
               if(ug->KeyPressed(10)||trickey=='9') b-=2;
               if(ug->KeyPressed(9)||trickey=='8') b-=4;
               if(ug->KeyPressed(8)||trickey=='7') b-=8;
               if(ug->KeyPressed(7)||trickey=='6') b-=16;
               break;
          case 0xDF:
               if(ug->KeyPressed(25)||ug->KeyPressed(40)||trickey=='P'||trickey=='"') b-=1;
               if(ug->KeyPressed(24)||trickey=='O'||trickey==')') b-=2;
               if(ug->KeyPressed(23)||trickey=='I'||trickey=='(') b-=4;
               if(ug->KeyPressed(22)||trickey=='U') b-=8;
               if(ug->KeyPressed(21)||trickey=='Y') b-=16;
               break;
          case 0xBF:
               if(ug->KeyPressed(28)||trickey=='\n') b-=1;
               if(ug->KeyPressed(38)||trickey=='L') b-=2;
               if(ug->KeyPressed(37)||trickey=='K') b-=4;
               if(ug->KeyPressed(36)||trickey=='J') b-=8;
               if(ug->KeyPressed(35)||trickey=='H') b-=16;
               break;
          case 0x7F:
               if(ug->KeyPressed(57)||trickey==' ') b-=1;
               if(ug->KeyPressed(97)||ug->KeyPressed(157)||ug->KeyPressed(40)||trickey==2||trickey=='"'||trickey==')'||trickey=='(') b-=2;
               if(ug->KeyPressed(50)||trickey=='M') b-=4;
               if(ug->KeyPressed(49)||trickey=='N') b-=8;
               if(ug->KeyPressed(48)||trickey=='B') b-=16;
               break;
        }
//        for(int i=0;i<LASTKEY;i++) if(ug->KeyPressed(i)) printf("KEY %i\n",i);
#else
#ifdef PRINTOUT
        printf("Read from Sinclair port 0x%4.4X\n",port);
#endif
#endif
        break;
   default:
        printf("Read from unknown port 0x%4.4X\n",port);
 }
 return b;
}

void writeport(WORD port,BYTE b)
{
 BYTE po = port & 0xFF;
 BYTE hi = port >> 8;
 FreqOut[po]++;
 switch(po)
 {
   case PORT_PAGE_0: // 0x82
#ifdef PRINTOUT
//        printf("set page 0 to %2.2X\n",b);
#endif
        ramw[0] = b;
        FreqPages[b]++;
        FreqMem[4]++;
        if(b>=0x50 && b<=0x5F) FreqMem[8]++;
        break;
   case PORT_PAGE_1: // 0xA2
        ramw[1] = b;
        FreqPages[b]++;
        FreqMem[5]++;
        if(b>=0x50 && b<=0x5F) FreqMem[9]++;
        break;
   case PORT_PAGE_2: // 0xC2
        ramw[2] = b;
        FreqPages[b]++;
        FreqMem[6]++;
        if(b>=0x50 && b<=0x5F) FreqMem[10]++;
        break;
   case PORT_PAGE_3: // 0xE2
        ramw[3] = b;
        FreqPages[b]++;
        FreqMem[7]++;
        if(b>=0x50 && b<=0x5F) FreqMem[11]++;
        break;
   case PORT_Y: // 0x89
        VideoY = b;
        break;
   case PORT_Y1: // 0x8A
        VideoY = b + 256;
        break;
   case 0x3C:
#ifdef PRINTOUT
        printf("no BIOS in page 0 (%2.2X)\n",b);
#endif
        BiosEnabled = 0;
        FreqMem[13]++;
        break;
   case 0x4E:
#ifdef PRINTOUT
        printf("port 0x4E = 0x%2.2X - COVOX control\n",b);
#endif
        break;
   case 0x4F:
#ifdef PRINTOUT
        printf("port 0x4F = 0x%2.2X - COVOX data\n",b);
#endif
        break;
   case 0x5C:
#ifdef PRINTOUT
        printf("port 0x5C = 0x%2.2X\n",b);
#endif
        break;
   case 0x7C:
#ifdef PRINTOUT
        printf("BIOS in page 0 (%2.2X)\n",b);
#endif
        BiosEnabled = 1;
        FreqMem[12]++;
        break;
   case 0xC9:
#ifdef PRINTOUT
        printf("port 0xC9 = 0x%2.2X - RGMOD\n",b);
#endif
        RGMOD = b;
//        if(VPage!=(b&1)) VideoSwap(Video);
//        VPage = b&1;
//        OffsetX = VPage*360;
        break;
   case 0xF4:
#ifdef PRINTOUT
        printf("port 0xF4 = 0x%2.2X - Spectrum SE?\n",b);
#endif
        break;
   case 0xFB:
#ifdef PRINTOUT
        printf("port 0xFB = 0x%2.2X - COVOX data (compatible)\n",b);
#endif
        break;
   case 0xFD:
        if(hi==0x1F)
        {
#ifdef PRINTOUT
           printf("system port 0x1FFD = 0x%2.2X\n",b);
#endif
           _1FFD = b;
#if 0
           if(b&1) BiosEnabled = 0;
           else BiosEnabled = 1;
#endif
        }
        else if(hi==0x7F)
        {
#ifdef PRINTOUT
            printf("Extended port 0x7FFD = 0x%2.2X\n",b);
#endif
           _7FFD = b;
#ifdef BASICSE
           ramw[3] = b&7;
           ramw[0] = (b&0x10)?11:10;
           if(b&8) VPageZX = 7;
           else    VPageZX = 5;
#else
#ifdef PRINTOUT
           printf("Extended port 0x7FFD = 0x%2.2X\n",b);
#endif
#endif
        }
        else if(hi==0xBF)
        {
#ifdef PRINTOUT
           printf("AY-data port 0xBFFD = 0x%2.2X\n",b);
#endif
        }
        else if(hi==0xFF)
        {
#ifdef PRINTOUT
           printf("AY-address port 0xFFFD = 0x%2.2X\n",b);
#endif
        }
        else printf("Write 0x%2.2X to unknown port 0x%4.4X\n",b,port);
        break;
   case 0xFE:
#ifdef PRINTOUT
        printf("Write to Sinclair port xxFE = 0x%2.2X\n",b);
#endif
        break;
   case 0xFF:
//#ifdef PRINTOUT
        printf("Write to port xxFF = 0x%2.2X\n",b);
//#endif
        _FF = b;
#ifdef BASICSE
        if((_FF&7)==6) SpectrumEnabled = 1;
#endif
        break;
   default:
        printf("Write 0x%2.2X to unknown port 0x%4.4X\n",b,port);
 }
}

void PrintChar(short x0,short y0,short ch,short cs,short cb,short f)
{
// printf("PrintChar(%i,%i,0x%2.2X (%c),%i,%i,%i)\n",x0,y0,ch,(ch<32)?' ':ch,cs,cb,f);
 int i,xx,yy;
 if(f)
 {
    int a = x0+y0*80;
    if(a>=0 && a<TEXTLEN)
    {
       if(ch) TextS[a] = ch;
       if(cs>=0)
       {
         if(cb<0) TextA[a] = cs;
         else TextA[a] = cs + (cb<<4);
       }
       ch = TextS[a];
       cs = TextA[a]&15;
       cb = TextA[a]>>4;
    }
 }
 if(hFont==8)
 {
   xx = x0<<3;
   yy = y0<<3;
   if(!Extended)
      ug->DrawChar8x8(xx,yy,ch,cs,cb);
   else
   {
      yy = y0*10;
      if(ch>=0xB0 && ch<0xDF)
      {
        ug->DrawChar8x8(xx,yy,ch,cs,cb);
        ug->DrawChar8x8(xx,yy+2,ch,cs,cb);
        ug->DrawChar8x8(xx,yy+1,ch,cs,cb);
      }
      else
      {
        ug->DrawChar8x8(xx,yy+1,ch,cs,cb);
        for(i=0;i<8;i++)
        {
          ug->SetScreenPixel(xx+i,yy,cb);
          ug->SetScreenPixel(xx+i,yy+9,cb);
        }
      }
   }
 }
}

void SetPalette(int pal,short base,short len,short num)
{
 int i,n,r,g,b;
 n = num;
 if(len==0) len=256;
 for(i=0;i<len;i++)
 {
    b = readbyte(pal+(i<<2)+0);
    g = readbyte(pal+(i<<2)+1);
    r = readbyte(pal+(i<<2)+2);
    Palette[n][base+i].Red   = r;
    Palette[n][base+i].Green = g;
    Palette[n][base+i].Blue  = b;
#ifdef VIDEODEBUG
    if(fdebug!=NULL) fprintf(fdebug,"Palette[%i][%i] = #%2.2X%2.2X%2.2X ",n,base+i,r,g,b);
#endif
//    if(n==0)
    {
       if(base+i==255 && r==0 && g==0 && b==0)
       {
          r = 255;
          g = 255;
          b = 255;
       }
       if(!Extended || Video==0x81) ug->SetPalette(base+i,r,g,b);
    }
 }
 ug->Update();
}

void Scroll(int b,int e,int d,int l,int h,int a)
{
 int i,j,k;
 if(b==1) // up
 {
   for(j=d;j<d+h-1;j++){
   for(i=e;i<e+l;i++){
     k = i+j*80;
     TextS[k] = TextS[k+80];
     TextA[k] = TextA[k+80];
     PrintChar(i,j,TextS[k],TextA[k]);
    }}
    if(a==0)
    {
      for(i=e;i<e+l;i++)
      {
       k = i+j*80;
       TextS[k] = 0x20;
       PrintChar(i,j,TextS[k],TextA[k]);
      }
    }
 }
 if(b==2) // down
 {
    for(j=d+h-1;j>0;j--){
    for(i=e;i<e+l;i++){
       k = i+j*80;
       TextS[k] = TextS[k-80];
       TextA[k] = TextA[k-80];
       PrintChar(i,j,TextS[k],TextA[k]);
    }}
    if(a==0)
    {
      for(i=E;i<E+L;i++)
      {
         TextS[i] = 0x20;
         PrintChar(i,0,TextS[i],TextA[i]);
      }
    }
 }
}

int Accel(int a,int b,int adr)
{
 int i;
 if(fdebug!=NULL) fprintf(fdebug,"\tAccel '%c' 0x%2.2X 0x%4.4X mode=%i size=%i\n",a?a:' ',b,adr,AccelMode,AccelSize);
 AccelFlag = 1;

 switch(AccelMode) {

 case 0: // LD B,B - switch off
  AccelFlag = 0;
  return 0;

 case 1: // LD D,D - set size of accelerator buffer
  if(a=='R'||a=='W')
  { AccelSize = b;
    if(AccelSize==0) AccelSize=256;
  }
  break;

 case 2: // LD C,C - fill in memory
  if(a=='W'||a=='R')
  {
    for(i=0;i<AccelSize;i++) writebyte(adr+i,b);
    tstates += AccelSize;
  }
  break;

 case 3: // LD E,E - fill in video
  if(a=='W'||a=='R')
  {
    for(i=0;i<AccelSize;i++)
    {
        writebyte(adr,b);
        VideoY = (VideoY+1)%ScrDY;
    }
    tstates += AccelSize;
  }
  break;

case 4: // LD H,H - set higher byte of the size of acceleratior buffer (NEW!)
  if(Extended>0 && (a=='R'||a=='W'))
  {
    if(AccelSize==0 && b==0) AccelSize=65536;
    else AccelSize += b<<8;
  }
  break;

case 5: // LD L,L - copy in memory
  switch(a)
  {
   case 'R': for(i=0;i<AccelSize;i++) AccelRAM[i] = readbyte(adr+i);  break;
   case 'W': for(i=0;i<AccelSize;i++) writebyte(adr+i,AccelRAM[i]);   break;
   case '^': for(i=0;i<AccelSize;i++) AccelRAM[i] ^= readbyte(adr+i); break;
   case '|': for(i=0;i<AccelSize;i++) AccelRAM[i] |= readbyte(adr+i); break;
   case '&': for(i=0;i<AccelSize;i++) AccelRAM[i] &= readbyte(adr+i); break;
  }
  tstates += AccelSize;
  break;

case 6: // LD A,A - copy in video
  switch(a)
  {
   case 'R':
    for(i=0;i<AccelSize;i++)
    {
        AccelRAM[i] = readbyte(adr);
        VideoY =(VideoY+1)%ScrDY;
    }
    break;
   case 'W':
    for(i=0;i<AccelSize;i++)
    {
        writebyte(adr,AccelRAM[i]);
        VideoY = (VideoY+1)%ScrDY;
    }
    break;
   case '^':
    for(i=0;i<AccelSize;i++)
    {
        AccelRAM[i] ^= readbyte(adr);
        VideoY = (VideoY+1)%ScrDY;
    }
    break;
   case '|':
    for(i=0;i<AccelSize;i++)
    {
        AccelRAM[i] |= readbyte(adr);
        VideoY = (VideoY+1)%ScrDY;
    }
    break;
   case '&':
    for(i=0;i<AccelSize;i++)
    {
        AccelRAM[i] &= readbyte(adr);
        VideoY = (VideoY+1)%ScrDY;
    }
    break;
  }
  tstates += AccelSize;
  break;

 }
 AccelFlag = 0;
 return 1;
}

int SetVideo(int vid)
{
 int ok = 1;
 int clr = 0;
 int old = Video;
 printf("SetVideo #%2.2X from #%2.2X\n",vid,old);
 wFont = 8;
 hFont = 8;
 switch(vid)
 {
   case 0x02:
        if(!Extended)
        {
          ug->Screen(320,256);
          ScrDX = 320;
          ScrDY = 256;
        }
        else
        {
          ug->Screen(320,350);
          ScrDX = 320;
          ScrDY = 350;
          OffsetY = 0; // hack
          if(Extended) SetStdPalette(16); // ???
        }
        if(old>=0x80) clr=1;
        break;

   case 0x03:
        if(!Extended)
        {
          ug->Screen(640,256);
          ScrDX = 640;
          ScrDY = 256;
        }
        else
        {
          ug->Screen(640,350);
          ScrDX = 640;
          ScrDY = 350;
          OffsetY = 0; // hack
          if(Extended) SetStdPalette(16);
        }
        if(old>=0x80) clr=1;
        break;

   case 0x81:
        if(!Extended)
        {
          ug->Screen(320,256);
          ScrDX = 320;
          ScrDY = 256;
        }
        else
        {
          ug->Screen(320,350);
          ScrDX = 320;
          ScrDY = 350;
          OffsetY = 50; // hack
        }
        if(old!=0x81) clr=1;
        break;

   case 0x82:
        if(!Extended)
        {
          ug->Screen(640,256);
          ScrDX = 640;
          ScrDY = 256;
        }
        else
        {
          ug->Screen(640,350);
          ScrDX = 640;
          ScrDY = 350;
          OffsetY = 50; // hack
          if(Extended) SetStdPalette(16);
        }
        if(old!=0x82) clr=1;
        break;

   case 0x83:
        if(!Extended)
        {
          printf("Video Mode 0x83 is not supported in Sprinter!\n");
          vid = old;
        }
        else
        {
          if(old<=0x80) clr=1;
          else VideoBackup(old);
          ug->Screen(320,200);
          ScrDX = 320;
          ScrDY = 200;
          OffsetY = 0; // hack
          if(!clr) VideoRestore(vid);
        }
        break;

   case 0x84:
        if(!Extended)
        {
          printf("Video Mode 0x84 is not supported in Sprinter!\n");
          vid = old;
        }
        else
        {
          if(old<=0x80) clr=1;
          else VideoBackup(old);
          ug->Screen(640,350);
          ScrDX = 640;
          ScrDY = 350;
          OffsetY = 0; // hack
          if(Extended) SetStdPalette(16);
          if(!clr) VideoRestore(vid);
        }
        break;

   case 0x85:
        if(!Extended)
        {
          printf("Video Mode 0x85 is not supported in Sprinter!\n");
          vid = old;
        }
        else
        {
          if(old<=0x80) clr=1;
          else VideoBackup(old);
          clr=0;
          ug->Screen(320,350);
          ScrDX = 320;
          ScrDY = 350;
          OffsetY = 0; // hack
          if(Extended && old==0x84) SetStdPalette(256);
          if(!clr) VideoRestore(vid);
        }
        break;

   case 0x86:
        if(!Extended)
        {
          printf("Video Mode 0x86 is not supported in Sprinter!\n");
          vid = old;
        }
        else
        {
          if(old<=0x80) clr=1;
          else VideoBackup(old);
          ug->Screen(640,400);
          ScrDX = 640;
          ScrDY = 400;
          OffsetY = 0; // hack
          if(Extended) SetStdPalette(16);
          if(!clr) VideoRestore(vid);
        }
        break;

   case 0x87:
        if(!Extended)
        {
          printf("Video Mode 0x87 is not supported in Sprinter!\n");
          vid = old;
        }
        else
        {
          if(old<=0x80) clr=1;
          else VideoBackup(old);
          ug->Screen(320,400);
          ScrDX = 320;
          ScrDY = 400;
          OffsetY = 0; // hack
          if(Extended && old==0x86) SetStdPalette(256);
          if(!clr) VideoRestore(vid);
        }
        break;

   default:
        printf("Unknown Video Mode 0x%2.2X\n",vid);
        ok = 0;
        break;
 }
 if(clr)
 {
   for(int j=0;j<ScrDY;j++){
   for(int i=0;i<ScrDX;i++){
     ug->SetScreenPixel(i,j,0);
   }}
 }
 Video = vid;
 return ok;
}

void ScreenShot(void)
{
 int i,j,k,c=0,o=0;
 int dx=ScrDX,dy=ScrDY,sx=1,sy=1;
 char file[300];
 sprintf(file,"%sSp%4.4d.png",ExePath,++SnapLast); // ExePath added in Nov 2021
 if(SnapDouble)
 {
    if(dx < 640){dx<<=1;sx<<=1;}
    dy<<=1;sy<<=1;
 }
 unsigned char* img = (unsigned char*)malloc(3*dx*dy);
 if(img==NULL) return;
 for(j=0;j<dy;j++){
 for(i=0;i<dx;i++){
   if(!(i%sx)) c = ug->GetScreenPixel(i/sx,j/sy);
   k = ug->GetPalette(c);
   img[o++] = k&255;
   img[o++] = (k>>8)&255;
   img[o++] = (k>>16)&255;
 }}
#ifdef PRINTOUT
 printf("Screenshot %s (%ix%i)\n",file,dx,dy);
#endif
 SavePNG(file,dx,dy,img);
 free(img);
}

void MemorySave(int n)
{
 char fn[100];
 if(n>0)
     sprintf(fn,"memory%i.bin",n);
 else if(n<0)
      strcpy(fn,"memoryal.bin");
 else strcpy(fn,"memory.bin");
 FILE *f = UniFopen(fn,"wb");
 if(f==NULL) return;
 if(n<0) fwrite(ram,PAGSIZE,256,f);
 else for(int i=0;i<65536;i++) fputc(readbyte(i),f);
 fclose(f);
#ifdef PRINTOUT
 printf("Memory saved to %s\n",fn);
#endif
}

void SwitchMode(void)
{
 int old = Video;
 if(Video>=0x84 && Video<=0x87)
 {
   if(Video&1) SetVideo(Video&0xFE);
   else SetVideo(Video|1);
#ifdef PRINTOUT
   printf("Video mode switched form #%2.2X to #%2.2X\n",old,Video);
#endif
 }
 else
 {
#ifdef PRINTOUT
   printf("Video mode can not be switched for #%2.2X\n",Video);
#endif
 }
 ug->Update();
}

void SetPixel(short x,short y,short c)
{
 ug->SetScreenPixel(x,y,c);
}

short GetPixel(short x,short y)
{
 return ug->GetScreenPixel(x,y);
}

void SetStdPalette(int col)
{
  int i,j,k,a,b;
  ug->SetPalette ( 0,0x00,0x00,0x00);
  ug->SetPalette ( 1,0x00,0x00,0xAA);
  ug->SetPalette ( 2,0x00,0xAA,0x00);
  ug->SetPalette ( 3,0x00,0xAA,0xAA);
  ug->SetPalette ( 4,0xAA,0x00,0x00);
  ug->SetPalette ( 5,0xAA,0x00,0xAA);
  ug->SetPalette ( 6,0xAA,0x55,0x00); // ug->SetPalette ( 6,0xAA,0xAA,0x00); // "brown correction"
  ug->SetPalette ( 7,0xAA,0xAA,0xAA);
  ug->SetPalette ( 8,0x55,0x55,0x55);
  ug->SetPalette ( 9,0x55,0x55,0xFF);
  ug->SetPalette (10,0x55,0xFF,0x55);
  ug->SetPalette (11,0x55,0xFF,0xFF);
  ug->SetPalette (12,0xFF,0x55,0x55);
  ug->SetPalette (13,0xFF,0x55,0xFF);
  ug->SetPalette (14,0xFF,0xFF,0x55);
  ug->SetPalette (15,0xFF,0xFF,0xFF);
  if(col==256)
  {
//    ug->SetPalette ( 6,0xAA,0xAA,0x00); // just for test how it will be without "brown correction"
    for(k=16;k<256;k++)
    {
      i = k>>4;
      j = k&15;
      a = ug->GetPalette(i);
      b = ug->GetPalette(j);
      a = (a&0xFEFEFE)>>1;
      b = (b&0xFEFEFE)>>1;
      a += b;
      ug->SetPalette(k,a&0xFF,(a>>8)&0xFF,(a>>16)&0xFF);
    }
    for(k=0;k<16;k++)
    {
      a = ug->GetPalette(k);
      a = (a&0xFEFEFE)>>1;
      ug->SetPalette(k,a&0xFF,(a>>8)&0xFF,(a>>16)&0xFF);
    }
#if 0
    int r,g,freq[256];
    unsigned int u;
    for(k=0;k<256;k++) freq[k]=0;
    printf("\n");
    for(k=0;k<256;k++)
    {
      u = (unsigned int)ug->GetPalette(k);
      r = u&0xFE;
      if(r==0xA8) r+=2;
      if(r==0xFE) r++;
      g = (u>>8)&0xFE;
      if(g==0xA8) g+=2;
      if(g==0xFE) g++;
      b = (u>>16)&0xFE;
      if(b==0xA8) b+=2;
      if(b==0xFE) b++;
      freq[r]++;
      freq[g]++;
      freq[b]++;
//      printf("pal[%i]=0x%2.2X%2.2X%2.2X;",k,(unsigned int)r,(unsigned int)g,(unsigned int)b);
      printf("%3d %3d %3d\t%i",r,g,b,k);
/*
      #define BIN(x,m) for(i=0;i<m;i++){printf("%c",(x&(1<<(m-1)))?'1':'0');x<<=1;};
      printf("\t// ");
      a=k;BIN(a,8);printf(" => ");
      a=r/0x2A;BIN(a,3);printf(" ");
      a=g/0x2A;BIN(a,3);printf(" ");
      a=b/0x2A;BIN(a,3);
*/
      printf("\n");
    }
    printf("\n");
    for(k=0;k<256;k++)
      if(freq[k]) printf("freq[0x%2.2X]=%i\n",k,freq[k]);
    printf("\n");
#endif
  }
}

void Print256(const char *s, int *a)
{
 int mi,ma,i,j;
 ma = 0;
 mi = -1;
 for(i=0;i<256;i++)
 {
   if(a[i])
   {
//      printf("%2.2X=%i\n",i,a[i]);
      if(mi<0) mi=i;
      else ma=i;
   }
 }
// printf("%2.2X...%2.2X\n",mi,ma);
 printf("\n%s:\n",s);
 if(mi<0) printf("EMPTY\n");
 else
 {
   printf("  :");
   for(i=0;i<16;i++) printf("%1.1X",i);
   printf("\n");
   for(j=mi>>4;j<=ma>>4;j++)
   {
      printf("%1.1XX:",j);
      for(i=0;i<16;i++)
        if(a[(j<<4)|i]) printf("*");
        else printf(" ");
      printf("\n");
   }
 }
}

// 0-pos 1-char 2-shift 3-rus 4-rus+shift 5-numlock
static unsigned char _keys_[256][6] = {
{0x00,0x00,0x00,0x00,0x00,0x00}, // 00h (0)
{0x01,0x1B,0x1B,0x1B,0x1B,0xFF}, // 01h (1) - Esc
{0x02, '1', '!', '1', '!',0xFF}, // 02h (2)
{0x03, '2', '@', '2', '"',0xFF}, // 03h (3)
{0x04, '3', '#', '3', '#',0xFF}, // 04h (4)
{0x05, '4', '$', '4', ';',0xFF}, // 05h (5)
{0x06, '5', '%', '5', ':',0xFF}, // 06h (6)
{0x07, '6', '^', '6', ',',0xFF}, // 07h (7)
{0x08, '7', '&', '7', '.',0xFF}, // 08h (8)
{0x09, '8', '*', '8', '*',0xFF}, // 09h (9)
{0x0A, '9', '(', '9', '(',0xFF}, // 0Ah (10)
{0x0B, '0', ')', '0', ')',0xFF}, // 0Bh (11)
{0x0C, '-', '_', '-', '_',0xFF}, // 0Ch (12)
{0x0D, '=', '+', '=', '+',0xFF}, // 0Dh (13)
{0x0E,0x08,0x08,0x08,0x08,0xFF}, // 0Eh (14) - BackSpace
{0x0F,0x09,0x09,0x09,0x09,0xFF}, // 0Fh (15) - Tab
{0x10, 'q', 'Q', 'q', 'Q',0xFF}, // 10h (16)
{0x11, 'w', 'W', 'w', 'W',0xFF}, // 11h (17)
{0x12, 'e', 'E', 'e', 'E',0xFF}, // 12h (18)
{0x13, 'r', 'R', 'r', 'R',0xFF}, // 13h (19)
{0x14, 't', 'T', 't', 'T',0xFF}, // 14h (20)
{0x15, 'y', 'Y', 'y', 'Y',0xFF}, // 15h (21)
{0x16, 'u', 'U', 'u', 'U',0xFF}, // 16h (22)
{0x17, 'i', 'I', 'i', 'I',0xFF}, // 17h (23)
{0x18, 'o', 'O', 'o', 'O',0xFF}, // 18h (24)
{0x19, 'p', 'P', 'p', 'P',0xFF}, // 19h (25)
{0x1A, '[', '{', '[', '{',0xFF}, // 1Ah (26)
{0x1B, ']', '}', ']', '}',0xFF}, // 1Bh (27)
{0x28,0x0D,0x0D,0x0D,0x0D,0xFF}, // 28h (28) - Enter
{0x36,0x00,0x00,0x00,0x00,0x00}, // 36h (29) - Left Ctrl
{0x1D, 'a', 'A', 'a', 'A',0xFF}, // 1Dh (30)
{0x1E, 's', 'S', 's', 'S',0xFF}, // 1Eh (31)
{0x1F, 'd', 'D', 'd', 'D',0xFF}, // 1Fh (32)
{0x20, 'f', 'F', 'f', 'F',0xFF}, // 20h (33)
{0x21, 'g', 'G', 'g', 'G',0xFF}, // 21h (34)
{0x22, 'h', 'H', 'h', 'H',0xFF}, // 22h (35)
{0x23, 'j', 'J', 'j', 'J',0xFF}, // 23h (36)
{0x24, 'k', 'K', 'k', 'K',0xFF}, // 24h (37)
{0x25, 'l', 'L', 'l', 'L',0xFF}, // 25h (38)
{0x26, ';', ':', ';', ':',0xFF}, // 26h (39)
{0x27,0x27, '"',0x27, '"',0xFF}, // 27h (40) - '
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (41) - SCANCODE_GRAVE
{0x29,0x00,0x00,0x00,0x00,0x00}, // 29h (42) - Left Shift
{0x35,0x2F, '|',0x2F, '|',0xFF}, // 35h (43) - Backslash
{0x2A, 'z', 'Z', 'z', 'Z',0xFF}, // 2Ah (44)
{0x2B, 'x', 'X', 'x', 'X',0xFF}, // 2Bh (45)
{0x2C, 'c', 'C', 'c', 'C',0xFF}, // 2Ch (46)
{0x2D, 'v', 'V', 'v', 'V',0xFF}, // 2Dh (47)
{0x2E, 'b', 'B', 'b', 'B',0xFF}, // 2Eh (48)
{0x2F, 'n', 'N', 'n', 'N',0xFF}, // 2Fh (49)
{0x30, 'm', 'M', 'm', 'M',0xFF}, // 30h (50)
{0x31, ',', '<', ',', '<',0xFF}, // 31h (51)
{0x32, '.', '>', '.', '>',0xFF}, // 32h (52)
{0x33, '/', '?', '/', '/',0xFF}, // 33h (53)
{0x34,0x00,0x00,0x00,0x00,0x00}, // 34h (54) - Right Shift
{0x4B, '*', '*', '*', '*', '*'}, // 4Bh (55) - Gray Mul
{0x37,0x00,0x00,0x00,0x00,0x00}, // 37h (56) - Left Alt
{0x38, ' ', ' ', ' ', ' ',0xFF}, // 38h (57) - Space
{0x1C,0x00,0x00,0x00,0x00,0x00}, // 1Ch (58) - CapsLock
{0x3B,0x00,0x00,0x00,0x00,0x00}, // 3Bh (59) - F1
{0x3C,0x00,0x00,0x00,0x00,0x00}, // 3Ch (60) - F2
{0x3D,0x00,0x00,0x00,0x00,0x00}, // 3Dh (61) - F3
{0x3E,0x00,0x00,0x00,0x00,0x00}, // 3Eh (62) - F4
{0x3F,0x00,0x00,0x00,0x00,0x00}, // 3Fh (63) - F5
{0x40,0x00,0x00,0x00,0x00,0x00}, // 40h (64) - F6
{0x41,0x00,0x00,0x00,0x00,0x00}, // 41h (65) - F7
{0x42,0x00,0x00,0x00,0x00,0x00}, // 42h (66) - F8
{0x43,0x00,0x00,0x00,0x00,0x00}, // 43h (67) - F9
{0x44,0x00,0x00,0x00,0x00,0x00}, // 44h (68) - F10
{0x49,0x00,0x00,0x00,0x00,0x00}, // 49h (69) - Num Lock
{0x48,0x00,0x00,0x00,0x00,0x00}, // 48h (70) - Scroll Lock
{0x57,0x00,0x00,0x00,0x00, '7'}, // 57h (71) - Home 7
{0x58,0x00,0x00,0x00,0x00, '8'}, // 58h (72) - Up 8
{0x59,0x00,0x00,0x00,0x00, '9'}, // 59h (73) - PgUp 9
{0x4C, '-', '-', '-', '-', '-'}, // 4Ch (74) - Gray Minus
{0x54,0x00,0x00,0x00,0x00, '4'}, // 54h (75) - Left 4
{0x55,0x00,0x00,0x00,0x00, '5'}, // 55h (76) - 5
{0x56,0x00,0x00,0x00,0x00, '6'}, // 56h (77) - Right 6
{0x4D, '+', '+', '+', '+', '+'}, // 4Dh (78) - Gray Plus
{0x51,0x00,0x00,0x00,0x00, '1'}, // 51h (79) - End 1
{0x52,0x00,0x00,0x00,0x00, '2'}, // 52h (80) - Down 2
{0x53,0x00,0x00,0x00,0x00, '3'}, // 53h (81) - PgDn 3
{0x50,0x00,0x00,0x00,0x00, '0'}, // 50h (82) - Ins 0
{0x4F,0x00,0x00,0x00,0x00, '.'}, // 4Fh (83) - Del .
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (84) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (85) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (86) - SCANCODE_LESS
{0x45,0x00,0x00,0x00,0x00,0x00}, // 45h (87) - F11
{0x46,0x00,0x00,0x00,0x00,0x00}, // 46h (88) - F12
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (89) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (90) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (91) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (92) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (93) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (94) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (95) - ?
{0x4E,0x0D,0x0D,0x0D,0x0D,0x0D}, // 4Eh (96) - Enter
{0x3A,0x00,0x00,0x00,0x00,0x00}, // 3Ah (97) - Right Ctrl
{0x4A, '/', '/', '/', '/', '/'}, // 4Ah (98) - Gray Div
{0x47,0x00,0x00,0x00,0x00,0x00}, // 47h (99) - Print Screen
{0x39,0x00,0x00,0x00,0x00,0x00}, // 39h (100) - Right Alt
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (101) - SCANCODE_BREAK
{0x57,0x00,0x00,0x00,0x00,0x00}, // ??h (102) - SCANCODE_HOME
{0x58,0x00,0x00,0x00,0x00,0x00}, // ??h (103) - SCANCODE_CURSORBLOCKUP
{0x59,0x00,0x00,0x00,0x00,0x00}, // ??h (104) - SCANCODE_PAGEUP
{0x54,0x00,0x00,0x00,0x00,0x00}, // ??h (105) - SCANCODE_CURSORBLOCKLEFT
{0x56,0x00,0x00,0x00,0x00,0x00}, // ??h (106) - SCANCODE_CURSORBLOCKRIGHT
{0x51,0x00,0x00,0x00,0x00,0x00}, // ??h (107) - SCANCODE_END
{0x52,0x00,0x00,0x00,0x00,0x00}, // ??h (108) - SCANCODE_CURSORBLOCKDOWN
{0x53,0x00,0x00,0x00,0x00,0x00}, // ??h (109) - SCANCODE_PAGEDOWN
{0x50,0x00,0x00,0x00,0x00,0x00}, // ??h (110) - SCANCODE_INSERT
{0x4F,0x00,0x00,0x00,0x00,0x00}, // ??h (111) - SCANCODE_REMOVE
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (112) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (113) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (114) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (115) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (116) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (117) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (118) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (119) - SCANCODE_BREAK_ALTERNATIVE
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (120) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (121) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (122) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (123) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (124) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (125) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (126) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (127) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (128) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (129) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (130) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (131) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (132) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (133) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (134) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (135) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (136) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (137) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (138) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (139) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (140) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (141) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (142) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (143) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (144) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (145) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (146) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (147) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (148) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (149) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (150) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (151) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (152) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (153) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (154) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (155) - ?
{0x4E,0x0D,0x0D,0x0D,0x0D,0x0D}, // 4Eh (156) - Gray Enter
{0x3A,0x00,0x00,0x00,0x00,0x00}, // 3Ah (157) - Right Ctrl
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (158) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (159) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (160) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (161) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (162) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (163) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (164) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (165) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (166) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (167) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (168) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (169) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (170) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (171) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (172) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (173) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (174) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (175) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (176) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (177) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (178) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (179) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (180) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (181) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (182) - ?
{0x47,0x00,0x00,0x00,0x00,0x00}, // 47h (183) - Print Screen
{0x39,0x00,0x00,0x00,0x00,0x00}, // 39h (184) - Right Alt
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (185) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (186) - ?
{0x4A, '/', '/', '/', '/', '/'}, // 4Ah (187) - Gray Div
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (188) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (189) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (190) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (191) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (192) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (193) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (194) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (195) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (196) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (197) - Pause
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (198) - ?
{0x57,0x00,0x00,0x00,0x00,0x00}, // ??h (199) - Home
{0x58,0x00,0x00,0x00,0x00,0x00}, // ??h (200) - Up
{0x59,0x00,0x00,0x00,0x00,0x00}, // ??h (201) - Page Up
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (202) - ?
{0x54,0x00,0x00,0x00,0x00,0x00}, // ??h (203) - Left
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (204) - ?
{0x56,0x00,0x00,0x00,0x00,0x00}, // ??h (205) - Right
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (206) - ?
{0x51,0x00,0x00,0x00,0x00,0x00}, // ??h (207) - End
{0x52,0x00,0x00,0x00,0x00,0x00}, // ??h (208) - Down
{0x53,0x00,0x00,0x00,0x00,0x00}, // ??h (209) - Page Down
{0x50,0x00,0x00,0x00,0x00,0x00}, // ??h (210) - Insert
{0x4F,0x00,0x00,0x00,0x00,0x00}, // ??h (211) - Delete
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (212) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (213) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (214) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (215) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (216) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (217) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (218) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (219) - ?
{0x00,0x00,0x00,0x00,0x00,0x00}, // ??h (220) - ?
};

int KeyRusLat = 0;
int KeyNumLock = 0;
int KeyScrollLock = 0;
int KeyInsert = 0;
int KeyCapsLock = 0;
int Update = 1;

// flag=0  - normal
// flag=1  - keep
// flag=-1 - clear buffer
int GetKey(int flag)
{
 int i,k=1;
 switch(flag)
 {
  case 0:
    k = ug->Update();
    Update = 1;
    break;
  case 1:
    if(Update)
    {  k = ug->Update();
       if(!k) 
          Update = 1;
       else
          Update = 0;
    }
    else  Update = 1;
    break;
  case -1:
    ug->KeyClear();
    return 0;
 }
 B = C = D = E = 0;
 if(!k) return 0;
 if(KeyRusLat)     C |= 0x80;
 if(KeyNumLock)    C |= 0x08;
 if(KeyScrollLock) C |= 0x04;
 if(KeyInsert)     C |= 0x02;
 if(KeyCapsLock)   C |= 0x01;
 int LeftShift  = (ug->KeyPressed(SCANCODE_LEFTSHIFT))?0x80:0;
 int RightShift = (ug->KeyPressed(SCANCODE_RIGHTSHIFT))?0x40:0;
 int LeftCtrl   = (ug->KeyPressed(SCANCODE_LEFTCONTROL))?0x08:0;
 int RightCtrl  = (ug->KeyPressed(SCANCODE_RIGHTCONTROL))?0x02:0;
 int LeftAlt    = (ug->KeyPressed(SCANCODE_LEFTALT))?0x04:0;
 int RightAlt   = (ug->KeyPressed(SCANCODE_RIGHTALT))?0x01:0;
 int Ctrl       = (RightCtrl&&LeftCtrl)?0x20:0;
 int Alt        = (RightAlt&&LeftAlt)?0x10:0;
 int Shift      = (RightShift&&LeftShift);
 B = LeftShift|RightShift|Ctrl|Alt|LeftCtrl|LeftAlt|RightCtrl|RightAlt;
 for(i=0;i<LASTKEY;i++)
 {
    if(i==SCANCODE_LEFTSHIFT) continue;
    if(i==SCANCODE_RIGHTSHIFT) continue;
    if(i==SCANCODE_LEFTCONTROL) continue;
    if(i==SCANCODE_RIGHTCONTROL) continue;
    if(i==SCANCODE_LEFTALT) continue;
    if(i==SCANCODE_RIGHTALT) continue;
    if(ug->KeyPressed(i)) break;
 }
 if(i==LASTKEY) i=0;
 D = _keys_[i][0];
 if(B) D |= 0x80;
 if(Shift)
    E = _keys_[i][2];
 else
    E = _keys_[i][1];
 if(D||E)
 {
  if(fdebug!=NULL) fprintf(fdebug,"\n>>> KBD #%2.2X #%2.2X (%c)\n",D,E,(E>32)?E:' ');
//  if(E&&!flag)
    delay(120);
 }
 return E;
}

MouseState* GetMouseState(void)
{
 static MouseState m;
 m.x = ug->GetMouseX();
 m.y = ug->GetMouseY();
 m.br = ug->GetMouseR();
 m.bm = ug->GetMouseM();
 m.bl = ug->GetMouseL();
 return &m;
}

int SavePNG(char* filename, int width, int height, unsigned char* data)
{
#ifdef PNGOK
   /* Assume that it's RGB888 image */
   int bitdepth = 8;
   int colortype = PNG_COLOR_TYPE_RGB;
   int transform = PNG_TRANSFORM_IDENTITY;
   int i = 0;
   int r = 0;
   FILE* fp = NULL;
   png_structp png_ptr = NULL;
   png_infop info_ptr = NULL;
   static png_bytep row_pointers[1024];
   if(NULL == data) { r = -1; goto endlabel; }
   if(!filename || !filename[0]) { r = -2; goto endlabel; }
   if(NULL == (fp = fopen(filename, "wb"))) { r = -4; goto endlabel; }
   if(NULL == (png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL))) { r = -5; goto endlabel; }
   if(NULL == (info_ptr = png_create_info_struct(png_ptr))) { r = -6; goto endlabel; }
   png_set_IHDR(png_ptr, info_ptr, width, height, bitdepth,
                colortype,          /* PNG_COLOR_TYPE_{GRAY, PALETTE, RGB, RGB_ALPHA, GRAY_ALPHA, RGBA, GA} */
                PNG_INTERLACE_NONE, /* PNG_INTERLACE_{NONE, ADAM7 } */
                PNG_COMPRESSION_TYPE_BASE,
                PNG_FILTER_TYPE_BASE);
   for(i = 0; i < height; ++i) row_pointers[i] = data + i*width*3;
   png_init_io(png_ptr, fp);
   png_set_rows(png_ptr, info_ptr, row_pointers);
   png_write_png(png_ptr, info_ptr, transform, NULL);
 endlabel:
   if(NULL != fp)
   {
     fclose(fp);
     fp = NULL;
   }
   if(NULL != png_ptr)
   {
     if(NULL == info_ptr) r = -7;
     png_destroy_write_struct(&png_ptr, &info_ptr);
   }
   return r;
#else
   return 0;
#endif
}
