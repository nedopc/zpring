CC = g++
CFLAGS = `sdl-config --cflags` -I. `libpng-config --cflags` -Wall -O2 -DSDL -DNETWORK
LFLAGS = `sdl-config --libs` -lm `libpng-config --libs`
OBJS = network.o unigraf.o bios.o z80/z80.o z80/z80_ops.o

all: zpring

clean:
	rm -f *.o
	rm -f z80/*.o

z80/z80.o: z80/z80.cpp z80/z80.h
	$(CC) $(CFLAGS) -c z80/z80.cpp -o z80/z80.o

z80/z80_ops.o: z80/z80_ops.cpp z80/z80.h z80/z_cb.hpp z80/z_ddfd.hpp z80/z_ddfdcb.hpp z80/z_ed.hpp z80/z_macros.h
	$(CC) $(CFLAGS) -c z80/z80_ops.cpp -o z80/z80_ops.o

bios.o: bios.cpp bios.h zpring.h
	$(CC) $(CFLAGS) -c bios.cpp

network.o: network.cpp bios.h zpring.h
	$(CC) $(CFLAGS) -c network.cpp

unigraf.o: unigraf.cpp unigraf.h
	$(CC) $(CFLAGS) -c unigraf.cpp -DFONT8X8

zpring: zpring.cpp bios.h zpring.h z80/z80.h $(OBJS)
	$(CC) zpring.cpp $(CFLAGS) -o zpring $(LFLAGS) $(OBJS)
