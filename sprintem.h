/*  sprintem.h - defines for SprintEm (initially created on Feb 18, 2002).

    This file is part of SprintEm (emulator of Sprinter computer and clones).

    Copyright (c) 2002-2022, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _SPRINTEM_H
#define _SPRINTEM_H

typedef signed char SBYTE;
typedef unsigned char BYTE;
typedef signed short SWORD;
typedef unsigned short WORD;
typedef signed int SDWORD;
typedef unsigned int UDWORD;

#define PORT_PAGE_0 0x82
#define PORT_PAGE_1 0xA2
#define PORT_PAGE_2 0xC2
#define PORT_PAGE_3 0xE2
#define PORT_Y      0x89
#define PORT_Y1     0x8A /* Extended */

extern unsigned long tstates;
extern unsigned long tevent;

BYTE readbyte(WORD address,BYTE flag=1);
BYTE writebyte(WORD address,BYTE data);
BYTE readport(WORD port);
void writeport(WORD port,BYTE b);

#define RAMSIZE 4194304 // 4M
#define PAGSIZE 16384 // 16K

typedef struct _Page
{ BYTE occ;
  BYTE num;
  BYTE *data;
} Page;

#define TEXTLEN 2560

extern BYTE *ram;
extern int ramw[4];
extern Page *ramo;
extern int RamPages;
extern int ScrDX,ScrDY;
extern int Exit;
extern int Video;
extern int VPage;
extern int OffsetX;
extern int AccelMode;
extern BYTE *TextS;
extern BYTE *TextA;
extern int BiosEnabled;
extern int EstexEnabled;
extern int SpectrumEnabled;
extern int Extended;
extern int FreqEstex[256];
extern int FreqBios[256];
extern int FreqMouse[256];
extern int FreqOut[256];
extern int FreqIn[256];
extern int FreqPages[256];
extern int FreqMem[14]; // SETWINx4,PORTSx4,VIDPAGx4,BIOS,noBIOS
extern char ExePath[256];

int Accel(int a,int b,int adr);
void PrintChar(short x0,short y0,short ch,short cs=-1,short cb=-1,short f=1);
void SetPalette(int,short,short,short);
void SetPixel(short,short,short);
short GetPixel(short,short);
void Scroll(int b,int e,int d,int l,int h,int a=0);
int SetVideo(int);
void ScreenShot(void);
int GetKey(int flag=0);
void MemorySave(int n=0);
void SwitchMode(void);

int hex2i(char *s);

#ifdef NETWORK
int Network(int command);
int NetworkInit(const char*);
#endif

struct MouseState
{
 short x,y;
 unsigned char bl,bm,br;
};

MouseState* GetMouseState(void);

FILE* UniFopen(const char *name, const char *fmode);

struct SprintemRGB
{
 unsigned char Red;
 unsigned char Green;
 unsigned char Blue;
 SprintemRGB(short r=0, short g=0, short b=0) { Red=r; Green=g; Blue=b; };
};

#endif

