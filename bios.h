/*  bios.h - Defines for Sprinter BIOS&DSS emulation (created on Feb 18, 2002).

    This file is part of SprintEm (emulator of Sprinter computer and clones).

    Copyright (c) 2002-2021, Alexander Shabarshin <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _BIOS_H
#define _BIOS_H

#pragma pack(1)

typedef struct _SprintEXE_
{
  char id[3];
  unsigned char version;
  unsigned int offset;
  unsigned short loader;
  unsigned short loader2;
  unsigned short reserv1;
  unsigned short reserv2;
  unsigned short load;
  unsigned short start;
  unsigned short stack;
  unsigned char res[490];
} SprintEXE;

#pragma pack()

extern int wFont;
extern int hFont;
extern int CurX;
extern int CurY;
extern int ColS;
extern int ColB;
extern int Level;

extern char *Path;
extern char PathA[64];

extern int BoardID;

int Init(void);
int Bios(int);
int Estex(int);
int Mouse(int);
int TestAddress(int adr);
int Find(int adrbuf,int adrnam=-1,int atr=0,int flag=1);

#endif
