REM This is a build script for Borland C++ v5.5 that is available as a freeware
REM You can take freecommandLinetools.exe from here https://downloads.embarcadero.com/item/24778
REM You can change SDL v1.2 location below:
SET SDL=C:\SRC\SDL
DEL ..\*.obj
DEL ..\z80\*.obj
DEL ..\sprintem.exe
CD ..\z80
bcc32.exe -DWIN95 -DSDL -DNETWORK -c -O2 z80.cpp
bcc32.exe -DWIN95 -DSDL -DNETWORK -c -O2 z80_ops.cpp
CD ..
bcc32.exe -DWIN95 -DSDL -DNETWORK -c -O2 bios.cpp
bcc32.exe -DWIN95 -DSDL -DNETWORK -c -O2 network.cpp
bcc32.exe -I%SDL%\include -DWIN95 -c -O2 -DFONT8X8 -DSDL -DUNIFOPEN unigraf.cpp 
bcc32.exe -tW -I%SDL%\include -L%SDL% -DWIN95 -DSDL -DNETWORK -esprintem.exe sprintem.cpp SDLmain.lib SDL.lib z80\z80.obj z80\z80_ops.obj bios.obj unigraf.obj network.obj
