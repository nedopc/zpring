;    CP/M BIOS Emulation
;
;    Copyright (c) 2002,2021 A.A.Shabarshin <me@shaos.net>
;
;    This file is part of CPMEMUL for Sprinter computer.
;
;    CPMEMUL is free software; you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation; either version 2 of the License, or
;    (at your option) any later version.
;
;    CPMEMUL is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with CPMEMUL; if not, write to the Free Software
;    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


	include sp.inc

	org	bdos

	incbin	zsdos-gp.bin

myout	equ	1
iobyte	equ	0003h
curdsk	equ	0004h
sp_dx	equ	80
sp_dy	equ	32
_numpg	equ	0C0F0h
_page1	equ	0C0F1h
_page2	equ	0C0F2h
_page3	equ	0C0F3h
_page4	equ	0C0F4h
_pagesp	equ	0C0FEh
_estex	equ	0C0FFh

bios_label:

	jp	_boot
	jp	_wboot
	jp	_const
	jp	_conin
	jp	_conout
	jp	_list
	jp	_punch
	jp	_reader
	jp	_home
	jp	_seldsk
	jp	_settrk
	jp	_setsec
	jp	_setdma
	jp	_read
	jp	_write
	jp	_listst
	jp	_sectran
	
pagesv	db	0
curatr	db	2
temp	db	0
pagesp	db	0
savesp	dw	0
curx	db	0
cury	db	0

dma	dw	0080h
track	dw	0
sector	dw	0
	
_boot:	
	ld	SP,0E000h
	call	KBD_INIT
if	myout
	call	cls
endif

_wboot:	; we need CCP
	in	a,(PORT_PAGE1)
	ld	(pagesv),a
	ld	a,(_pagesp)
	ld	(pagesp),a
	ld	a,(_page1)
	out	(PORT_PAGE1),a
	ld	hl,4000h
	ld	de,0000h
	ld	bc,4000h
	ldir
	ld	a,(_numpg)
	cp	1
	jp	z,wboot1
	; ???
wboot1:	ld	a,(pagesv)
	out	(PORT_PAGE1),a
	jp	100h
	
_const:	call	TEST_KBD
	jr	nz,_con1
	xor	a
	ret
_con1:	ld	a,0FFh
	ret
	
_conin: call	READ_KBD
	jr	c,_conin
	ret
	
_conout:
	push	hl
	push	de
	push	bc
if	myout
	ld	a,c
	call	print1
else
	ld	a,(curatr)
	ld	e,a
	ld	a,c
	ld	b,1
	ld	c,BIOS_LP_PRINT_ALL
	rst	8
endif	
	pop	bc
	pop	de
	pop	hl
	ret
	
_list:	ret
	
_punch:	ret
	
_reader:
	xor	a
	ret
	
_home:	push	hl
	ld	hl,0
	ld	(track),hl
	pop	hl
	ret
	
_seldsk:
	ld	a,c
	ld	(curdsk),a
	
	
	
	ret
	
_settrk:
	push	hl
	push	bc
	pop	hl
	ld	(track),hl
	pop	hl
	ret
	
_setsec:
	push	hl
	push	bc
	pop	hl
	ld	(sector),hl
	pop	hl
	ret
	
_setdma:
	push	hl
	push	bc
	pop	hl
	ld	(dma),hl
	pop	hl
	ret
	
_read:
	ret
	
_write:
	ret
	
_listst:
	xor	a
	ret
	
_sectran:


	ret


; PRINT1
; c - symbol
print1: ld	a,c
	cp	13
	jr	nz,pr1a
	xor	a
	ld	(curx),a
	jr	pr1z
pr1a:	cp	10
	jr	nz,pr1b
	jp	pr1y
pr1b:
pr1m:	in	a,(PORT_PAGE1)
	ld	(temp),a
	ld	a,50h
	out	(PORT_PAGE1),a
	ld	a,(curx)
	ld	b,129
	add	a,b
	out	(PORT_Y),a
	ld	hl,4000h
	ld	de,769
	add	hl,de
	ld	d,0
	ld	a,(cury)
	ld	e,a
	add	hl,de
	add	hl,de
	add	hl,de
	add	hl,de
	ld	(hl),c
	inc	hl
	ld	a,(curatr)
	ld	(hl),a
	ld	a,(temp)
	out	(PORT_PAGE1),a
	ld	a,(curx)
	inc	a
	cp	sp_dx
	jr	nz,pr1x
	xor	a
	ld	(curx),a
pr1y:	ld	a,(cury)
	inc	a
	cp	sp_dy
	jr	nz,pr1yy
	call	scroll ; new scroll should include lastcls
	xor	a
	ld	(curx),a
;	call	lastcls
	jr	pr1z
pr1x:	ld	(curx),a
	jr	pr1z
pr1yy:	ld	(cury),a
pr1z:	ret

_ok:	; c - coord Y
	in	a,(PORT_PAGE1)
	ld	(temp),a
	ld	a,50h
	out	(PORT_PAGE1),a
	ld	a,129
	out	(PORT_Y),a
	ld	hl,4000h
	ld	de,769
	add	hl,de
	ld	b,0
	add	hl,bc
	add	hl,bc
	add	hl,bc
	add	hl,bc
	ld	a,(curatr)
	ld	c,a
	ld	(hl),'o'
	inc	hl
	ld	(hl),c
	dec	hl
	ld	a,130
	out	(PORT_Y),a
	ld	(hl),'k'
	inc	hl
	ld	(hl),c
	dec	hl
	ld	a,131
	out	(PORT_Y),a
sym:	ld	(hl),'!'
	inc	hl
	ld	(hl),c
	dec	hl
	ld	a,132
	out	(PORT_Y),a
	ld	a,(temp)
	out	(PORT_PAGE1),a
	ret

; CLS

cls:	in	a,(PORT_PAGE1)
	ld	(temp),a
	ld	a,80
	out	(PORT_PAGE1),a
	ld	d,sp_dx
	ld	b,129
cls1:	ld	e,sp_dy
	ld	a,b
	out	(PORT_Y),a
	ld	hl,4000h+769
cls2:	ld	(hl),20h
	inc	hl
	ld	a,(curatr)
	ld	(hl),a
	inc	hl
	inc	hl
	inc	hl
	dec	e
	jr	nz,cls2
	inc	b
	dec	d
	jr	nz,cls1
	ld	a,(temp)
	out	(PORT_PAGE1),a
	ret

; FASTCLS - Thanks to Ivan Mak (forth.exe)

fastcls:
	LD D,D
	LD A,	#50
	LD B,B
	LD E,	#20
	LD A,(curatr)
	LD D,A
	IN A,(PORT_PAGE1)
	EX AF,AF';'
	LD A,	#50
	OUT (PORT_PAGE1),A
	LD HL,	#4301
	LD A,	#80
	AND	#80
	INC A
fcloop: OUT (PORT_Y),A
	LD E,E
	LD (HL),E ; !!!
	LD B,B
	INC L
	OUT (PORT_Y),A
	LD E,E
	LD (HL),D ; !!!
	LD B,B
	INC L
	INC L
	INC L
	BIT 7,l
	JR Z,fcloop
	EX AF,AF';'
	OUT (PORT_PAGE1),A
	ret

; LASTCLS

lastcls:
	LD D,D
	LD A,	#50
	LD B,B
	LD E,	#20
	LD A,(curatr)
	LD D,A
	IN A,(PORT_PAGE1)
	EX AF,AF';'
	LD A,	#50
	OUT (PORT_PAGE1),A
	LD HL,	#4301
	LD BC,	31
	ADD	HL,BC
	ADD	HL,BC
	ADD	HL,BC
	ADD	HL,BC
	LD A,	#80
	INC A
	OUT (PORT_Y),A
	LD E,E
	LD (HL),E ; !!!
	LD B,B
	INC L
	OUT (PORT_Y),A
	LD E,E
	LD (HL),D ; !!!
	LD B,B
	EX AF,AF';'
	OUT (PORT_PAGE1),A
	ret

; SCROLL + cleraing last line

scroll:
	in	a,(PORT_PAGE1)
	ld	(temp),a
	ld	a,50h
	out	(PORT_PAGE1),a
	ld	a,129
scro1:	out	(PORT_Y),a
	ld	hl,4305h
	ld	de,4301h
	ld	b,32
	push	af
scro2:	ld	a,(hl)
	ld	(de),a
	inc	hl
	inc	de
	ld	a,(hl)
	ld	(de),a
	inc	hl
	inc	de
	inc	hl
	inc	de
	inc	hl
	inc	de
	djnz	scro2
	pop	af
	dec	hl
	dec	hl
	dec	hl
	dec	hl
	ld	(hl),20h
	inc	a
	cp	209
	jr	nz,scro1
	ld	a,(temp)
	out	(PORT_PAGE1),a
	ret


; SCROLL with Accel

fastscroll:
	in	a,(PORT_PAGE1)
	ld	(temp),a
	ld	a,50h
	out	(PORT_PAGE1),a
	ld	hl,4000h+769+4
	ld	de,4000h+769
	ld	c,32
	ld	d,d
	ld	a,80
	ld	b,b
	ld	a,129
scrl1:	out	(PORT_Y),a
	ld	a,a
	ld	b,(hl)
	ld	b,b
	ex	de,hl
	out	(PORT_Y),a
	ld	a,a
	ld	(hl),b
	ld	b,b
	ex	de,hl
	inc	l
	inc	e
	out	(PORT_Y),a
	ld	a,a
	ld	b,(hl)
	ld	b,b
	ex	de,hl
	out	(PORT_Y),a
	ld	a,a
	ld	(hl),b
	ld	b,b
	ex	de,hl
	inc	l
	inc	l
	inc	l
	inc	e
	inc	e
	inc	e
	dec	c
	jr	nz,scrl1
	ld	a,(temp)
	out	(PORT_PAGE1),a
	ret

; Keboard (not yet working well)

COM_A   EQU     19h
DAT_A   EQU     18h

KBD_INIT:
        LD A,0
        OUT (COM_A),A
        LD A,1
        OUT (COM_A),A
        LD A,0
        OUT (COM_A),A
        LD A,3
        OUT (COM_A),A
        LD A,0C1h
        OUT (COM_A),A
        LD A,4
        OUT (COM_A),A
        LD A,5h
        OUT (COM_A),A
        LD A,5
        OUT (COM_A),A
        LD A,062H
        OUT (COM_A),A
        RET

; Z - empty buffer

TEST_KBD:
        IN A,(COM_A)
        BIT 0,A
        RET Z

; C - error

READ_KBD:
        IN A,(COM_A)
        BIT 0,A
        SCF
        RET Z
        IN A,(DAT_A)
        AND A
        RET

