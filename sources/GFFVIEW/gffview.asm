; GFFVIEW.ASM - viewer of Sprinter video memory dumps by Shaos (March-May 2021)

; This code is PUBLIC DOMAIN - use it on your own RISK!

	org   #7F00

;EXE-file header:
	dw 5845h     ; EXE Signature
	db 45h	     ; Reserved (EXE type)
	db 00h	     ; Version of EXE file
	dw 0200h     ; Code offset
	dw 0000h
	dw 0000h     ; Primary loader size or 0 (no primary loader)
	dw 0000h     ; Reserved
	dw 0000h     ; Reserved
	dw 0000h     ; Reserved
	dw START     ; Loading address
	dw START     ; Starting address (register PC)
	dw ENDSP     ; Stack address (register SP)
	ds 490	     ; Reserved
	      
PAGE0	EQU	#82
PAGE1	EQU	#A2
PAGE2	EQU	#C2
PAGE3	EQU	#E2
PORT_Y	EQU	#89

;code of program

START:

	push	ix

; Write welcome message

	ld	hl,msg
	ld	c,5ch
	rst	10h

	ld	bc,113Dh ; allocate 17 pages
	rst	10h
	ld	(mem),a
	ld	bc,103Bh ; page 16 to window 3
	rst	10h
	ld	a,(mem)
	ld	bc,0039h ; page 0 to window 1
	rst	10h

; Write filename

	ld	hl,msg2
	ld	c,5ch
	rst	10h
	pop	hl
gff1:	inc	hl
	ld	a,(hl)
	cp	20h
	jr	z,gff1
	push	hl
	ld	c,5ch
	rst	10h
	ld	hl,msg1
	ld	c,5ch
	rst	10h
	pop	hl

; Check if it's GFX or GFF file - exit with error if neither

	push	hl
	ld	b,0
gff10:	ld	a,(hl)
	cp	0
	jr	z,gff11
	inc	hl
	cp	'.'
	jr	nz,gff10
	push	hl
	pop	ix
	ld	a,(ix+0)
	cp	'g'
	jr	z,gff10g
	cp	'G'
	jr	nz,gff11
gff10g:	ld	a,(ix+1)
	cp	'f'
	jr	z,gff10f
	cp	'F'
	jr	nz,gff11
gff10f:	ld	a,(ix+2)
	cp	'x'
	jr	z,gff10x
	cp	'X'
	jr	nz,gff10a
gff10x:	ld	a,(ix+3)
	cp	' '
	jr	z,gff10e
	or	a
	jr	nz,gff11
gff10e: xor	a
	ld	(ix+3),a
	ld	b,1
	jr	gff12
gff10a:	cp	'f'
	jr	z,gff10y
	cp	'F'
	jr	nz,gff11
gff10y:	ld	a,(ix+3)
	cp	' '
	jr	z,gff10z
	or	a
	jr	nz,gff11
gff10z:	xor	a
	ld	(ix+3),a
	ld	b,2
	jr	gff12
gff11:	ld	a,b
	or	a
	jr	nz,gff12
	ld	hl,msg3
	ld	c,5ch
	rst	10h
	pop	hl
	jp	gff0
gff12:	pop	hl
	push	bc

; Open file

	ld	a,1
	ld	c,11h
	rst	10h
	pop	bc
	jp	c,gff0
	ld	(fil),a
	ld	a,b
	cp	2
	jr	z,gff20

; Read GFX (uncompressed)

	ld	de,#4000
	ld	hl,#4000
	ld	b,0
gff2:	push	hl
	push	de
	push	bc
	ld	a,(fil)
	ld	c,13h
	rst	10h
	pop	bc
	pop	de
	pop	hl
	jp	c,gff0
	inc	b
	ld	a,16
	cp	b
	jr	z,gff3
	push	hl
	push	de
	push	bc
	ld	a,'.'
	call	print_char
	pop	bc
	push	bc
	ld	a,(mem)
	ld	c,39h ; window 1
	rst	10h
	pop	bc
	pop	de
	pop	hl
	jr	gff2
gff3:	call	print_newline
	jp	gffclose

; Read GFF (compressed)

gff20:	ld	de,12
	ld	hl,#C000
	ld	b,0
	ld	a,(fil)
	ld	c,13h
	rst	10h
	jp	c,gff0
	ld	ix,#C000
	ld	a,(ix+0)
	cp	'S'
	jp	nz,gff0
	ld	a,(ix+1)
	cp	'H'
	jp	nz,gff0
	ld	a,(ix+2)
	cp	'A'
	jp	nz,gff0
	ld	a,(ix+3)
	cp	'F'
	jp	nz,gff0
	ld	a,(ix+4)
	cp	'F'
	jp	nz,gff0
	ld	a,(ix+5)
	cp	'1'
	jp	nz,gff0
	ld	de,#4000
	ld	hl,#C000
	ld	b,0
gff21:	push	bc
	ld	a,(fil)
	ld	c,13h
	rst	10h
	ld	(red),de
	pop	bc
	jp	c,gff0
	push	bc
	ld	hl,#C000
	push	hl
	ld	de,#4000
	call	DESHAFF1
	jr	nc,gff22
	ld	bc,0041h
	rst	10h
gff22:	ex	de,hl
	xor	a
	ld	h,a
	ld	l,a
	sbc	hl,de
	ld	b,h
	ld	c,l
	ex	de,hl
	pop	de
	ldir
	xor	a
	ld	h,a
	ld	l,a
	sbc	hl,de
	ex	de,hl
	pop	bc
	inc	b
	ld	a,16
	cp	b
	jr	z,gff23
	push	hl
	push	de
	push	bc
	ld	a,'.'
	call	print_char
	pop	bc
	push	bc
	ld	a,(mem)
	ld	c,39h ; window 1
	rst	10h
	pop	bc
	pop	de
	pop	hl
	jr	gff21
gff23:	call	print_newline

; Close file

gffclose:
	ld	a,(fil)
	ld	c,12h
	rst	10h

; Save old video mode and set mode #81

	ld	c,#51
	rst	10h
	ld	c,a
	push	bc

	ld	a,81h
	ld	c,#50
	ld	b,0
	rst	10h

; Copy GFX dump to video memory

	ld	bc,0039h ; window 1
gff4:	push	bc
	ld	a,(mem)
	rst	10h ; set next page to window 1
	ld	a,50h
	out	(PAGE3),a
	ld	bc,1000h
gff5:	pop	hl
	push	hl
	push	bc
	ld	l,b
	xor	a
	ld	a,h
	rla
	rla
	rla
	rla
	ld	h,a
	ld	a,10h
	sub	l
	add	a,h
	out	(PORT_Y),a
	ld	b,c
	rrc	b
	rrc	b
	ld	c,0
	ld	de,#C000
	ld	hl,#4000
	add	hl,bc
	ld	bc,1024
	ldir
	pop	bc
	ld	a,16
	add	a,c
	ld	c,a
	djnz	gff5
	pop	bc
	inc	b
	ld	a,10h
	cp	b
	jr	nz,gff4
	

; Wait keypress

	ld	c,30h
	rst	10h

; Return old video mode

	pop	bc
	ld	a,c
	ld	c,#50
	rst	10h

; Exit program
    
gff0:	ld	bc,0041h
	rst	10h

print_hex:
	push	af
	rra
	rra
	rra
	rra
	and	15
	cp	10
	jr	nc,L1
	add	a,'0'
	jr	L2
L1	add	a,'A'-10
L2	ld	c,5Bh
	rst	10h
	pop	af
	and	15
	cp	10
	jr	nc,L3
	add	a,'0'
	jr	L4
L3	add	a,'A'-10
L4	ld	c,5Bh
	rst	10h
	ret

print_space:
	ld	a,32 
print_char:
	ld	c,5Bh
	rst	10h
	ret

print_newline:
	ld	a,13
	ld	c,5Bh
	rst	10h
	ld	a,10
	ld	c,5Bh
	rst	10h
	ret

msg	db	13,10,"GFFVIEW v0.3 (07-MAY-2021) by Shaos (PUBLIC DOMAIN)",13,10
msg1	db	13,10,0
msg2	db	"Filename: ",0
msg3	db	13,10,"ERROR: File must be GFX or GFF",13,10,0
mem	db	0
fil	db	0
red	dw	0

	include "deshaff1.asm"

	ds	256

ENDSP:	db	0
