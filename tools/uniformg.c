/* Generator of UNIFORM 256-color palette */

/* Created by Shaos in 2013 and updated in 2023 */

/* This code is PUBLIC DOMAIN - use it on your own risk! */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEST

#ifdef TEST
long pal[252]; /* UNIFORM676 only */
int dx,dy;
FILE *fxpm;
char xpmname[100],*po;
char digits[] = " .+@#$%&*=-;>,')!~{]^/(_:<[}|1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`";
#endif

int main(int argc, char** argv)
{
 int i,j,k,m,r,g,b;
 FILE *f = fopen("Uniform676G.gpl","wt");
 if(f==NULL) return -1;
 fprintf(f,"GIMP Palette\n");
 fprintf(f,"Name: UNIFORM-676-G6\n#\n");
 m = 0;
 for(i=0;i<6;i++){
 for(j=0;j<7;j++){
 for(k=0;k<6;k++){
   r = k*51;
   g = (int)(j*42.5);
   b = i*51;
#ifdef TEST
   pal[m] = (r<<16)|(g<<8)|b;
#endif
   fprintf(f,"%3d %3d %3d\t%i\n",r,g,b,m++);
 }}}
 i = 51;
 while(m<256)
 {
   fprintf(f,"%3d %3d %3d\t%i\n",i,i,i,m++);
   i+=51;
 }
 fclose(f);
#ifdef TEST
 /* Test conversion of full color PPM file to XPM with UNIFORM676 palette */
 if(argc>1 && (f=fopen(argv[1],"rb")))
 {
   i = fgetc(f);
   j = fgetc(f);
   strcpy(xpmname,argv[1]);
   po = strrchr(xpmname,'.');
   if(po!=NULL) *po = 0;
   strcat(xpmname,".xpm");
   fxpm = fopen(xpmname,"wt");
   if(fxpm==NULL||i!='P'||j!='6'){fclose(f);return -3;}
   b = ' ';
   while(b==' '||b=='\t'||b=='\n') b = fgetc(f);
   if(b=='#') while(b!='\n') b = fgetc(f);
   while(b==' '||b=='\t'||b=='\n') b = fgetc(f);
   dx = b-'0';
   dx = dx*10+fgetc(f)-'0';
   dx = dx*10+fgetc(f)-'0';
   b = ' ';
   while(b==' '||b=='\t'||b=='\n') b = fgetc(f);
   dy = b-'0';
   dy = dy*10+fgetc(f)-'0';
   dy = dy*10+fgetc(f)-'0';
   b = ' ';
   while(b==' '||b=='\t'||b=='\n') b = fgetc(f);
   i = fgetc(f);
   j = fgetc(f);
   if(b!='2'||i!='5'||j!='5'){fclose(f);fclose(fxpm);return -4;}
   b = ' ';
   while(b==' '||b=='\t'||b=='\n') b = fgetc(f);
   for(i=0;i<strlen(xpmname);i++) if(xpmname[i]=='.') xpmname[i] = '_';
   fprintf(fxpm,"/* XPM */\n");
   fprintf(fxpm,"static char * %s[] = {\n",xpmname);
   fprintf(fxpm,"\"%i %i 252 2\",\n",dx,dy);
   i = j = 0;
   for(k=0;k<252;k++)
   {
     fprintf(fxpm,"\"%c%c \tc #%6.6X\",\n",digits[i],digits[j],pal[k]);
     pal[k] = (digits[i]<<8)|digits[j];
     if(!digits[++i]){i=0;j++;}
   }
   for(j=0;j<dy;j++)
   {
     fprintf(fxpm,"\"");
     for(i=0;i<dx;i++)
     {
       if(i==0&&j==0) r = b; /* initial value was already read */
       else r = fgetc(f);
       g = fgetc(f);
       b = fgetc(f);
/* Precise formula: NDEX=INT((R+25.5)/51)+INT((G+21.25)/42.5)*6+INT((B+25.5)/51)*42 */
       k = ((int)((r+25)/51))+((int)((g+g+43)/85))*6+((int)((b+25)/51))*42;
       fprintf(fxpm,"%c%c",pal[k]>>8,pal[k]&255);
     }
     if(j<dy-1) fprintf(fxpm,"\",\n");
     else fprintf(fxpm,"\"};\n");
   }
   fclose(fxpm);
   fclose(f);
 }
#endif
 return 0;
}
